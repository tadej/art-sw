#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""Base class for grid and (local) build submits."""
from __future__ import print_function

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import calendar
import collections
import fnmatch
import glob
import json
import logging
import os
import re
import sys

try:
    import scandir as scan
except ImportError:  # pragma: no cover
    import os as scan

from datetime import datetime, timedelta

from art_configuration import ArtConfiguration
# from art_diff import ArtDiff
from art_header import ArtHeader
from art_misc import build_script_directory, dict_added, dict_changed, dict_removed, get_release, run_command, split_release, xrdcp

MODULE = "art.base"


class ArtBase(object):
    """Base class for grid and (local) build submits."""

    def __init__(self):
        """Keep arguments."""

    def validate(self, script_directory):
        """Validate all tests in given script_directory."""
        log = logging.getLogger(MODULE)
        log.info("Validating script directory: %s", script_directory)
        directories = self.get_test_directories(script_directory.rstrip("/"))

        found_test = False
        valid = True
        for directory in directories.itervalues():
            files = self.get_files(directory, valid_only=False)
            # print(files)
            for fname in files:
                test_name = os.path.join(directory, fname)
                found_test = True
                valid &= ArtHeader(test_name).validate()

        if not found_test:
            log.warning('No scripts found in %s directory', script_directory)
            return 1

        if not valid:
            log.error('Some scripts have invalid headers or are not executable')
            return 1

        log.info("Scripts in %s directory are valid", script_directory)
        return 0

    def included(self, script_directory, job_type, index_type, nightly_release, project, platform):
        """Return map of art-include values for each test_name found."""
        directories = self.get_test_directories(script_directory.rstrip("/"))
        result = {}
        for directory in directories.itervalues():
            files = self.get_files(directory, job_type, index_type)
            for fname in files:
                test_name = os.path.join(directory, fname)
                if self.is_included(test_name, nightly_release, project, platform):
                    result[test_name] = ArtHeader(test_name).get(ArtHeader.ART_INCLUDE)
        return result

    def input_collect(self, job_type, run_all_tests, copy, release):
        """Collect all art-input."""
        log = logging.getLogger(MODULE)
        input = collections.defaultdict(dict)

        (nightly_release, project, platform) = split_release(release, platform='x86_64-slc6-gcc62-opt')
        script_directory = build_script_directory(None, nightly_release, project, platform)
        log.debug("SCRIPT_DIRECTORY = %s", script_directory)
        if script_directory is None:  # pragma: no cover
            log.critical("Cannot find valid script directory")
            exit(1)

        directories = self.get_test_directories(script_directory.rstrip("/"))
        for directory in directories.itervalues():
            files = self.get_files(directory, nightly_release=nightly_release, project=project, platform=platform, job_type=job_type, run_all_tests=run_all_tests, valid_only=True, run_on=False)
            for fname in files:
                test_name = os.path.join(directory, fname)
                header = ArtHeader(test_name)
                log.debug("Test %s", test_name)
                inds = header.get(ArtHeader.ART_INPUT)
                if inds is not None:
                    n_files = header.get(ArtHeader.ART_INPUT_NFILES)
                    input[inds]['max_files'] = max(input.get(inds, {}).get('max_files', 0), n_files)
                    input[inds][test_name] = n_files
                    log.debug("%s %d", inds, n_files)

        filename = '-'.join((nightly_release, project, platform, 'art-input')) + ".json"
        with open(filename, "wb") as outfile:
            json.dump(input, outfile, sort_keys=True, indent=4)

        if copy and copy != '.':
            xrdcp(filename, copy)

        return 0

    def input_merge(self, work_dir, ref_file):
        """TBD."""
        input = collections.defaultdict(dict)

        list = glob.glob(os.path.join(work_dir, '*-art-input.json'))
        for input_file in list:
            with open(input_file, "rb") as f:
                file = json.load(f)

                for key in dict_changed(file, input):
                    for subkey in dict_changed(file[key], input[key]):
                        input[key][subkey] = max(input[key][subkey], file[key][subkey])

                    for subkey in dict_added(file[key], input[key]):
                        input[key][subkey] = file[key][subkey]

                for key in dict_added(file, input):
                    input[key] = file[key]

        input_file = os.path.join(work_dir, 'art-input.json')
        with open(input_file, "wb") as outfile:
            json.dump(input, outfile, sort_keys=True, indent=4)

        return 0 if ref_file is None else self.input_diff(input_file, os.path.join(work_dir, ref_file))

    def input_diff(self, input_file, ref_file):
        """Compare art-input.json with a reference file."""
        with open(input_file, "rb") as f:
            input = json.load(f)

        with open(ref_file, "rb") as f:
            ref = json.load(f)

        result = 0
        for key in dict_added(input, ref):
            result = 1
            print('A', key, input[key]['max_files'])

        for key in dict_changed(input, ref):
            if input[key]['max_files'] != ref[key]['max_files']:
                result = 1
                print('C', key, ref[key]['max_files'], ' -> ', input[key]['max_files'])

        for key in dict_removed(input, ref):
            result = 1
            print('D', key, ref[key]['max_files'])

        if result:
            print('Added (A), Changed (C), Removed (D)')

        return result

    def config(self, package, nightly_release, project, platform, config):
        """Show configuration."""
        log = logging.getLogger(MODULE)
        config = ArtConfiguration(config)
        if package is None:
            log.info("%s", config.packages())
            return 0

        keys = config.keys(nightly_release, project, platform, ArtConfiguration.ALL)
        keys.update(config.keys(nightly_release, project, platform, package))
        for key in keys:
            log.info("'%s'-'%s'", key, config.get(nightly_release, project, platform, package, key))
        return 0

    #
    # Default implementations
    #
    def compare_ref(self, path, ref_path, files, txt_files, diff_pool, diff_root, entries=-1, mode='detailed', order_trees=False, out=None):
        """TBD."""
        result = 0

        file = sys.stdout if out is None else open(out, "w")

        if diff_pool:
            (exit_code, out, err, command, start_time, end_time, timed_out) = run_command(' '.join(("art-diff.py", "--diff-type=diff-pool", path, ref_path)))
            if exit_code != 0:  # pragma: no cover
                result |= exit_code
                print(err, file=file)
            print(out, file=file)

        if diff_root:
            (exit_code, out, err, command, start_time, end_time, timed_out) = run_command(' '.join(("art-diff.py", "--diff-type=diff-root", "--mode=" + mode, "--entries=" + str(entries), "--order-trees" if order_trees else "", (' '.join(('--file=' + s for s in files))), path, ref_path)))
            if exit_code != 0:  # pragma: no cover
                result |= exit_code
                print(err, file=file)
            print(out, file=file)

        if txt_files:
            (exit_code, out, err, command, start_time, end_time, timed_out) = run_command(' '.join(("art-diff.py", "--diff-type=diff-txt", (' '.join(('--file=' + s for s in txt_files))), path, ref_path)))
            if exit_code != 0:  # pragma: no cover
                result |= exit_code
                print(err, file=file)
            print(out, file=file)

        return result

    #
    # Protected Methods
    #
    @staticmethod
    def get_art_results(output):
        """
        Extract art-results.

        find all
        'art-result: x' or 'art-result: x name' or 'art-result: [x]'
        and append them to result list
        """
        result = []
        for line in output.splitlines():
            match = re.search(r"art-result: (\d+)\s*(.*)", line)
            if match:
                item = json.loads(match.group(1))
                name = match.group(2)
                result.append({'name': name, 'result': item})
            else:
                match = re.search(r"art-result: (\[.*\])", line)
                if match:
                    array = json.loads(match.group(1))
                    for item in array:
                        result.append({'name': '', 'result': item})

        return result

    def __run_on(self, nightly_tag, on_days):
        """Return True if this is a good day to runself."""
        log = logging.getLogger(MODULE)
        if on_days is None or len(on_days) == 0:
            return True

        # nightly tag format: 2017-02-26T2119
        # tagday offset by 3 hours, so Saturday 21:00 to Saturday 21:00 is Saturday
        fmt = '%Y-%m-%dT%H%M'
        offset = timedelta(hours=3)
        tagday = datetime.strptime(nightly_tag, fmt) + offset

        for on_day in on_days:
            # list of keys
            days = on_day.split(',')
            for day in days:
                if day == '':
                    continue

                day_name = calendar.day_name[tagday.weekday()].lower()
                week_day = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
                keywords = week_day
                week_end = ['saturday', 'sunday']
                keywords.extend(week_end)
                weekdays = 'weekdays'
                keywords.append(weekdays)
                weekends = 'weekends'
                keywords.append(weekends)
                odd_days = 'odd days'
                keywords.append(odd_days)
                even_days = 'even days'
                keywords.append(even_days)

                if day.lower() in keywords:
                    # Check for Monday, Tuesday, ...
                    if day.lower() in week_day and day.lower() == day_name:
                        return True

                    # weekends
                    elif day.lower() == weekends and day_name in week_end:
                        return True

                    # weekdays
                    elif day.lower() == weekdays and day_name not in week_end:
                        return True

                    # odd days
                    elif day.lower() == odd_days and tagday.day % 2 != 0:
                        return True

                    # even days
                    elif day.lower() == even_days and tagday.day % 2 == 0:
                        return True

                else:
                    # day number
                    try:
                        if int(day) == tagday.day:
                            return True
                    except Exception:
                        log.warn("Some item in art_runon list '['" + day + "']' is not an integer or valid keyword, skipped")

        return False

    def get_files(self, directory, job_type=None, index_type="all", nightly_release=None, project=None, platform=None, run_all_tests=False, valid_only=True, run_on=True, tests=[]):
        """
        Return a list of all test files matching 'test_*.sh' of given 'job_type', 'index_type' and nightly/project/platform.

        'job_type' can be 'grid' or 'build', given by the test

        'index_type' can be 'all', 'batch' or 'single'.

        Tests are further filtered by run_all_tests, valid_only, run_on and list of tests.

        Only the filenames are returned.
        """
        result = []
        if directory is not None:
            nightly_tag = get_release(directory)[3]
            files = os.listdir(directory)
            files.sort()
            for fname in files:

                # is not a test ?
                if not fnmatch.fnmatch(fname, 'test_*.sh') and not fnmatch.fnmatch(fname, 'test_*.py'):
                    continue

                test_name = os.path.join(directory, fname)
                header = ArtHeader(test_name)
                has_art_input = header.get(ArtHeader.ART_INPUT) is not None
                has_art_athena_mt = header.get(ArtHeader.ART_ATHENA_MT) > 0

                # SKIP if file is not valid
                if valid_only and not header.is_valid():
                    continue

                # SKIP if art-type not defined
                if header.get(ArtHeader.ART_TYPE) is None:
                    continue

                # SKIP if is not of correct type
                if job_type is not None and header.get(ArtHeader.ART_TYPE) != job_type:
                    continue

                # SKIP if is not included in nightly_release, project, platform
                if not run_all_tests and nightly_release is not None and not self.is_included(test_name, nightly_release, project, platform):
                    continue

                # SKIP if batch and does specify art-input or art-athena-mt
                if index_type == "batch" and (has_art_input or has_art_athena_mt):
                    continue

                # SKIP if single and does NOT specify art-input or art-athena-mt
                if index_type == "single" and not (has_art_input or has_art_athena_mt):
                    continue

                # SKIP if this is not a good day to run
                if run_on and nightly_tag and not self.__run_on(nightly_tag, header.get(ArtHeader.ART_RUNON)):
                    continue

                # SKIP if not in list of tests
                if tests and fname not in tests:
                    continue

                result.append(fname)

        return result

    def get_type(self, directory, test_name):
        """Return the 'job_type' of a test."""
        return ArtHeader(os.path.join(directory, test_name)).get(ArtHeader.ART_TYPE)

    def get_test_directories(self, directory):
        """
        Search from '<directory>...' for '<package>/test' directories.

        A dictionary key=<package>, value=<directory> is returned
        """
        result = {}
        for root, dirs, files in scan.walk(directory):
            # exclude some directories
            dirs[:] = [d for d in dirs if not d.endswith('_test.dir')]
            if root.endswith('/test'):
                package = os.path.basename(os.path.dirname(root))
                result[package] = root
        return result

    def get_list(self, directory, package, job_type, index_type):
        """Return a list of tests for a particular package."""
        test_directories = self.get_test_directories(directory)
        test_dir = test_directories[package]
        return self.get_files(test_dir, job_type, index_type)

    def is_included(self, test_name, nightly_release, project, platform):
        """Return true if a match is found for test_name in nightly_release, project, platform."""
        assert test_name is not None
        assert nightly_release is not None
        assert project is not None
        assert platform is not None

        patterns = ArtHeader(test_name).get(ArtHeader.ART_INCLUDE)

        for pattern in patterns:
            (nightly_release_pattern, project_pattern, platform_pattern) = split_release(pattern, nightly_release="*", project="*", platform="*-*-*-*")

            if fnmatch.fnmatch(nightly_release, nightly_release_pattern) and fnmatch.fnmatch(project, project_pattern) and fnmatch.fnmatch(platform, platform_pattern):
                return True
        return False
