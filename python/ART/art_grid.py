#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""Class for grid submission."""
from __future__ import print_function

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import atexit
import concurrent.futures
# import errno
import exceptions
import fnmatch
import glob
import httplib
import json
import logging
import multiprocessing
import os
import re
# requests not available on lxplus, import only when needed
# import requests
import shutil
import sys
import tarfile
import tempfile
import threading
import time
import yaml

from datetime import datetime

from art_base import ArtBase
from art_configuration import ArtConfiguration
from art_header import ArtHeader
from art_rucio import ArtRucio
from art_misc import CVMFS_DIRECTORY, build_script_directory, count_files, gunzip, ls, mkdir, make_executable, rm, run_command, run_command_parallel, search, touch, uncomment_python, uncomment_sh, xrdcp

MODULE = "art.grid"


def old_copy_job(art_directory, indexed_package, dst, unpack, tmp, seq):
    """
    Copy job to be run by executor.

    Needs to be defined outside a class.
    """
    log = logging.getLogger(MODULE)
    log.debug("job started %s %s %s %s %s %d", art_directory, indexed_package, dst, unpack, tmp, seq)
    (exit_code, out, err, command, start_time, end_time, timed_out) = run_command(' '.join((os.path.join(art_directory, './art.py'), "copy", "--dst=" + dst, "--unpack" if unpack else "", "--tmp=" + tmp, "--seq=" + str(seq), indexed_package)))
    log.debug("job ended %s %s %s %s %s %d", art_directory, indexed_package, dst, unpack, tmp, seq)

    print("Copy job run with Exit Code:", exit_code)
    print(out)
    print(err)
    sys.stdout.flush()

    return (indexed_package, exit_code, start_time, end_time)


class ArtGrid(ArtBase):
    """Class for grid submission."""

    EOS_OUTPUT_DIR = '/eos/atlas/atlascerngroupdisk/data-art/grid-output'

    JOB_REPORT = 'jobReport.json'
    JOB_REPORT_ART_KEY = 'art'
    INITIAL_RESULT_WAIT_INTERVAL = 30 * 60  # seconds, 30 mins
    RESULT_WAIT_INTERVAL = 15 * 60  # seconds, 15 mins
    KINIT_WAIT = 4  # 4 * RESULT_WAIT_INTERVAL, 1 hour

    def __init__(self, art_directory, nightly_release, project, platform, nightly_tag, script_directory=None, max_jobs=0, run_all_tests=False):
        """Keep arguments."""
        super(ArtGrid, self).__init__()
        self.art_directory = art_directory
        self.nightly_release = nightly_release
        self.nightly_release_short = re.sub(r"-VAL-.*", "-VAL", self.nightly_release)
        self.project = project
        self.platform = platform
        self.nightly_tag = nightly_tag
        self.script_directory = script_directory
        self.max_jobs = multiprocessing.cpu_count() * 3 if max_jobs <= 0 else max_jobs
        self.run_all_tests = run_all_tests

        self.rucio = ArtRucio(self.art_directory, self.nightly_release_short, self.project, self.platform)

    def status(self, status):
        """Print status for usage in gitlab-ci."""
        print('art-status:', status)

    def get_script_directory(self):
        """Return calculated script directory, sometimes overriden by commandline."""
        return build_script_directory(self.script_directory, self.nightly_release, self.project, self.platform, self.nightly_tag)

    def is_script_directory_in_cvmfs(self):
        """Return true if the script directory is in cvmfs."""
        return self.get_script_directory().startswith(CVMFS_DIRECTORY)

    def exit_if_no_script_directory(self):
        """Exit with ERROR is script directory does not exist."""
        log = logging.getLogger(MODULE)
        if not os.path.isdir(self.get_script_directory()):  # pragma: no cover
            log.critical('Script directory does not exist: %s', os.path.realpath(self.get_script_directory()))
            self.status('error')
            exit(1)

    def exit_if_outfile_too_long(self, outfile_test):
        """Exit with ERROR if outfile too long."""
        log = logging.getLogger(MODULE)
        MAX_OUTFILE_LEN = 132
        if len(outfile_test) > MAX_OUTFILE_LEN:  # pragma: no cover
            log.error('OutFile string length > %d: %s', MAX_OUTFILE_LEN, outfile_test)
            exit(1)

    def __copy_art(self, run_dir, tests):
        """Copy all art files to the run directory. Returns final test script directory to be used."""
        log = logging.getLogger(MODULE)
        run_script_dir = os.path.join(run_dir, 'art', 'scripts')
        run_python_dir = os.path.join(run_dir, 'art', 'python', 'ART')
        mkdir(run_script_dir)
        mkdir(run_python_dir)

        log.info("art_directory: %s", self.art_directory)

        # get the path of the python classes and support scripts
        art_script_directory = self.art_directory
        art_python_directory = os.path.join(self.art_directory, '..', 'python', 'ART')

        shutil.copy(os.path.join(art_script_directory, 'art.py'), run_script_dir)
        shutil.copy(os.path.join(art_script_directory, 'art-diff.py'), run_script_dir)
        shutil.copy(os.path.join(art_script_directory, 'art-internal.py'), run_script_dir)
        shutil.copy(os.path.join(art_script_directory, 'art-download.sh'), run_script_dir)
        shutil.copy(os.path.join(art_script_directory, 'art-rucio-download.sh'), run_script_dir)
        shutil.copy(os.path.join(art_script_directory, 'art-rucio-list-dids.sh'), run_script_dir)
        shutil.copy(os.path.join(art_python_directory, '__init__.py'), run_python_dir)
        shutil.copy(os.path.join(art_python_directory, 'art_base.py'), run_python_dir)
        shutil.copy(os.path.join(art_python_directory, 'art_build.py'), run_python_dir)
        shutil.copy(os.path.join(art_python_directory, 'art_configuration.py'), run_python_dir)
        shutil.copy(os.path.join(art_python_directory, 'art_grid.py'), run_python_dir)
        shutil.copy(os.path.join(art_python_directory, 'art_header.py'), run_python_dir)
        shutil.copy(os.path.join(art_python_directory, 'art_misc.py'), run_python_dir)
        shutil.copy(os.path.join(art_python_directory, 'art_rucio.py'), run_python_dir)
        shutil.copy(os.path.join(art_python_directory, 'docopt.py'), run_python_dir)
        shutil.copy(os.path.join(art_python_directory, 'docopt_dispatch.py'), run_python_dir)

        make_executable(os.path.join(run_script_dir, 'art.py'))
        make_executable(os.path.join(run_script_dir, 'art-diff.py'))
        make_executable(os.path.join(run_script_dir, 'art-internal.py'))
        make_executable(os.path.join(run_script_dir, 'art-download.sh'))
        make_executable(os.path.join(run_script_dir, 'art-rucio-download.sh'))
        make_executable(os.path.join(run_script_dir, 'art-rucio-list-dids.sh'))

        script_directory = self.get_script_directory()

        # copy a local test directory if needed (only for 'art grid')
        if not self.is_script_directory_in_cvmfs():
            script_directory = os.path.basename(os.path.normpath(self.get_script_directory()))
            target_directory = os.path.join(run_dir, script_directory)
            log.info("Copying script directory for grid submission to %s", target_directory)
            shutil.copytree(self.get_script_directory(), target_directory, ignore=self.include_patterns(tests))

        return script_directory

    def include_patterns(self, patterns):
        """
        Function that can be used as copytree() include parameter.

        Patterns is a sequence of glob-style patterns
        that are used to include files, directories are always included, and only 'test_*' files are ever removed.
        An empty patterns list is an include all.
        """
        def _ignore_patterns(path, names):
            keep = set(name for pattern in patterns for name in fnmatch.filter(names, pattern))
            ignore = set(name for name in names if name.startswith('test_') and name not in keep and not os.path.isdir(os.path.join(path, name)))
            return ignore if patterns else set()
        return _ignore_patterns

    def get_jedi_id(self, text):
        """Return Jedi Task Id or 0."""
        match = re.search(r"jediTaskID=(\d+)", text)
        return match.group(1) if match else -1

    def copy(self, indexed_package, dst, user, unpack=False, tmp=None, seq=0, keep_tmp=False):
        """Copy output from scratch area to eos area."""
        log = logging.getLogger(MODULE)
        tmp = tempfile.mkdtemp(prefix=indexed_package + '-') if tmp is None else tmp
        mkdir(tmp)

        if indexed_package is not None:
            return self.copy_package(indexed_package, dst, user, unpack, tmp, seq, keep_tmp)

        # make sure script directory exist
        self.exit_if_no_script_directory()

        # get the test_*.sh from the test directory
        test_directories = self.get_test_directories(self.get_script_directory())
        if not test_directories:
            log.warning('No tests found in directories ending in "test"')

        # copy results for all packages
        result = 0
        for indexed_package, root in test_directories.items():
            number_of_tests = len(self.get_files(root, "grid", "all", self.nightly_release, self.project, self.platform, self.run_all_tests))
            if number_of_tests > 0:
                result |= self.copy_package(indexed_package, dst, user, unpack, tmp, seq, keep_tmp)
        return result

    def copy_package(self, indexed_package, dst, user, unpack, tmp, seq, keep_tmp):
        """Copy package to dst."""
        log = logging.getLogger(MODULE)

        result = 0

        log.debug("Indexed Package %s", indexed_package)

        package = indexed_package.split('.')[0]
        nightly_tag = self.nightly_tag if seq == 0 else '-'.join((self.nightly_tag, str(seq)))
        dst_dir = os.path.join(dst, self.nightly_release, self.project, self.platform, nightly_tag, package)
        log.info("tmp_dir %s", tmp)
        log.info("dst_dir %s", dst_dir)

        table = self.rucio.get_table(user, indexed_package, self.nightly_tag, tmp=tmp)
        if not table:
            log.warning("Nothing to be copied")
            return result

        for entry in table:
            grid_index = entry['grid_index']
            log.debug("Grid Index %d", grid_index)
            single_index = entry['single_index']
            log.debug("Single Index %d", single_index)

            # get the test name
            test_name = entry['job_name']
            if test_name is None:
                log.warning("EXT0 JSON not found, TestName could not be deduced for test with grid_index %d and single_index %d", grid_index, single_index)
                continue
            log.debug("Test_name %s", test_name)

            json_file = os.path.join(tmp, entry['outfile'] + "_EXT0", self.__get_rucio_name(user, entry, 'json'))
            json_dst = dst_dir.replace('/', '.')
            json_copying = json_file + ".copying_to" + json_dst
            json_copied = json_file + ".copied_to" + json_dst

            if os.path.isfile(json_copied):
                log.debug("Already copied: %d %s", grid_index, test_name)
            elif os.path.isfile(json_copying):
                log.debug("Still copying:  %d %s", grid_index, test_name)
            else:
                touch(json_copying)

                # create test directory
                test_dir = os.path.join(tmp, test_name)
                suffix = '-' + str(entry['grid_index'] - 1) if entry['single_index'] > 0 and entry['grid_index'] > 1 else ''
                test_dir += suffix
                mkdir(test_dir)

                # copy art-job.json
                result |= self.copy_json(json_file, test_dir)

                # copy and unpack log
                result |= self.copy_log(user, package, test_name, grid_index, test_dir, unpack, tmp)

                # copy results and unpack
                result |= self.copy_results(user, package, test_name, grid_index, test_dir, unpack, tmp)

                # copy to eos
                result |= self.copy_to_dst(test_name + suffix, test_dir, dst_dir)

                if result == 0:
                    rm(json_copying)
                    touch(json_copied)

                # cleanup
                if not keep_tmp:
                    shutil.rmtree(test_dir)

        return result

    def copy_json(self, json_file, test_dir):
        """Copy json."""
        log = logging.getLogger(MODULE)
        log.info("Copying JSON: %s", json_file)
        shutil.copyfile(json_file, os.path.join(test_dir, ArtRucio.ART_JOB))
        return 0

    def copy_log(self, user, package, test_name, grid_index, test_dir, unpack, tmp):
        """Copy and unpack log file."""
        log = logging.getLogger(MODULE)
        log.info("Copying LOG: %s %s", package, test_name)

        if unpack:
            tmp_tgz = self.__get_tar(user, self.nightly_release, self.project, self.platform, self.nightly_tag, package, test_name, grid_index=grid_index, tmp=tmp, tar=False)
            if tmp_tgz is not None:
                tar = tarfile.open(tmp_tgz)
                log.info("Unpacking LOG: %s", test_dir)
                logdir = None
                for member in tar.getmembers():
                    # does not work: tar.extractall()
                    tar.extract(member, path=test_dir)
                    logdir = member.name.split('/', 2)[0]

                tar.close()

                # rename top level log dir to logs
                if logdir is not None:
                    os.rename(os.path.join(test_dir, logdir), os.path.join(test_dir, "tarball_logs"))

                os.remove(tmp_tgz)
        else:
            tmp_tgz = self.__get_tar(user, self.nightly_release, self.project, self.platform, self.nightly_tag, package, test_name, grid_index=grid_index, tmp=tmp, tar=False)
            if tmp_tgz is not None:
                tmp_tar = os.path.splitext(tmp_tgz)[0] + '.tar'
                gunzip(tmp_tgz, tmp_tar)
                os.remove(tmp_tgz)
                xrdcp(tmp_tar, test_dir)
                os.remove(tmp_tar)
        return 0

    def copy_results(self, user, package, test_name, grid_index, test_dir, unpack, tmp):
        """Copy results and unpack."""
        log = logging.getLogger(MODULE)
        log.info("Copying TAR: %s %s", package, test_name)

        if unpack:
            tmp_tar = self.__get_tar(user, self.nightly_release, self.project, self.platform, self.nightly_tag, package, test_name, grid_index=grid_index, tmp=tmp)
            if tmp_tar is not None:
                tar = tarfile.open(tmp_tar)
                log.info("Unpacking TAR: %s", test_dir)
                tar.extractall(path=test_dir)
                tar.close()
                os.remove(tmp_tar)
        else:
            tmp_tar = self.__get_tar(user, self.nightly_release, self.project, self.platform, self.nightly_tag, package, test_name, grid_index=grid_index, tmp=tmp)
            if tmp_tar is not None:
                xrdcp(tmp_tar, test_dir)
                os.remove(tmp_tar)

        return 1 if tmp_tar is None else 0

    def copy_to_dst(self, test_name, test_dir, dst_dir):
        """Copy to dst."""
        log = logging.getLogger(MODULE)

        # extra check if dst is already made
        dst_target = os.path.join(dst_dir, test_name)

        # create the directory
        if mkdir(dst_target) != 0:
            return 1

        exit_code = xrdcp(test_dir, dst_target)

        # check number of source files
        nSrc = count_files(test_dir)
        nDst = count_files(dst_target)

        if nDst == nSrc:
            log.info("Number of files in Src (%d) and Dst (%d) are equal for %s", nSrc, nDst, test_name)
        else:
            log.warning("Number of files in Src (%d) and Dst (%d) differ for %s", nSrc, nDst, test_name)

        return exit_code

    def copy_job(self, thread_id, indexed_package, dst, user, unpack, tmp, seq):
        """Copy job to be run by executor."""
        log = logging.getLogger(MODULE)
        threading.current_thread().name = str(thread_id).zfill(2)
        log.debug("job %d started %s %s %s %s %s %d", thread_id, self.art_directory, indexed_package, dst, unpack, tmp, seq)
        start_time = datetime.now()
        exit_code = self.copy(indexed_package, dst, user, unpack=unpack, tmp=tmp, seq=seq)
        end_time = datetime.now()
        log.debug("job %d ended %s %s %s %s %s %d", thread_id, self.art_directory, indexed_package, dst, unpack, tmp, seq)

        print("Copy job run with Exit Code:", exit_code)
        return (indexed_package, exit_code, start_time, end_time)

    def task_package(self, root, package, job_type, sequence_tag, user, inform_panda, no_action, config_file, tests):
        """Submit a single package."""
        log = logging.getLogger(MODULE)
        result = {}
        number_of_tests = len(self.get_files(root, job_type, "all", self.nightly_release, self.project, self.platform, self.run_all_tests, tests=tests))
        if number_of_tests > 0:
            self.status('included')
            log.info('root %s with %d jobs', root, number_of_tests)
            log.info('Handling %s for %s project %s on %s', package, self.nightly_release, self.project, self.platform)

            run_dir = os.path.join(self.submit_directory, package, 'run')
            script_directory = self.__copy_art(run_dir, tests)

            result = self.task(script_directory, package, job_type, sequence_tag, user, inform_panda, no_action, config_file)
        return result

    def task_list(self, job_type, sequence_tag, user, inform_panda, packages=[], no_action=False, wait_and_copy=True, config_file=None, wait=INITIAL_RESULT_WAIT_INTERVAL, tests=[]):
        """Submit a list of packages."""
        log = logging.getLogger(MODULE)
        log.info("Inform Panda %s", inform_panda)

        # job will be submitted from tmp directory
        self.submit_directory = tempfile.mkdtemp(dir='.')

        # make sure tmp is removed afterwards
        atexit.register(shutil.rmtree, self.submit_directory, ignore_errors=True)

        # make sure script directory exist
        self.exit_if_no_script_directory()

        # get the test_*.sh from the test directory
        test_directories = self.get_test_directories(self.get_script_directory())
        if not test_directories:
            log.warning('No tests found in directories ending in "test"')

        configuration = None if config_file is None else ArtConfiguration(config_file)

        results = {}

        if not packages:
            # submit tasks for all packages
            for package, root in test_directories.items():
                if configuration is not None and configuration.get(self.nightly_release, self.project, self.platform, package, 'exclude', False):
                    log.warning("Package %s is excluded", package)
                else:
                    results.update(self.task_package(root, package, job_type, sequence_tag, user, inform_panda, no_action, config_file, tests))
        else:
            # Submit list of packages
            for package in packages:
                if package in test_directories:
                    root = test_directories[package]
                    results.update(self.task_package(root, package, job_type, sequence_tag, user, inform_panda, no_action, config_file, tests))
                else:
                    log.warning("Package does not exist: %s", package)

        if no_action:
            log.info("--no-action specified, so not waiting for results")
            return 0

        if len(results) == 0:
            log.warning('No tests found, nothing was submitted.')
            return 0

        if not wait_and_copy:
            log.debug("No copying")
            return 0

        copies = self.task_results(results, sequence_tag, user, configuration, wait)
        return self.task_copies(copies)

    def task_results(self, results, sequence_tag, user, configuration, wait):
        """Wait for and pick up task results."""
        log = logging.getLogger(MODULE)

        executor = None
        copies = []
        seq = None
        thread_id = 0

        kinit_interval = ArtGrid.KINIT_WAIT  # ArtGrid.KINIT_WAIT * ArtGrid.RESULT_WAIT_INTERVAL
        result_wait_interval = wait
        final_states = ["done", "finished", "failed", "aborted", "broken"]
        tmp = tempfile.mkdtemp(prefix=sequence_tag + '-')
        while len(results) > 0:
            log.debug("No of Results %d", len(results))
            log.debug("Waiting... %d seconds", result_wait_interval)
            time.sleep(result_wait_interval)
            log.debug("Done Waiting")
            if result_wait_interval >= wait:
                result_wait_interval = wait / 3
                log.debug("Waiting interval changed to %d seconds", result_wait_interval)
            kinit_interval -= 1
            if kinit_interval <= 0:
                os.system("kinit -R")
                kinit_interval = ArtGrid.KINIT_WAIT

            log.info("Copies %d", len(copies))
            if executor is not None:
                # NOTE: uses internals of ThreadPoolExecutor (executor.__dict__)
                log.info("Copy queue max-jobs: %d", executor._max_workers)
                log.info("Copy queue pending: %d jobs", executor._work_queue.qsize())
                log.info("Copy queue threads: %d", len(executor._threads))

            # retrieve the status' of all the jedi_ids known to this nightly
            # we can execute this per IP (vm) 185 times per hour MAX before getting into problems.
            # currently we execute this every 10 minutes (per nightly) so 6 times per hour.
            nightly_status = self.__nightly_status()

            # force a copy of (batch) results since we are modifying results
            for jedi_id in list(results):
                package = results[jedi_id][0]
                # skip packages without copy
                if not configuration.get(self.nightly_release, self.project, self.platform, package, "copy"):
                    log.info("Copy not configured for %s - skipped", package)
                    del results[jedi_id]
                    continue

                # figure out the destination for the copy based on if the directory already exists, keep seq
                # final_target created as directory, but re-evaluated as dst_dir in copy, using seq
                if seq is None:
                    dst = configuration.get(self.nightly_release, self.project, self.platform, package, "dst", ArtGrid.EOS_OUTPUT_DIR)
                    dst_dir = os.path.join(dst, self.nightly_release, self.project, self.platform, self.nightly_tag)
                    final_target = dst_dir
                    max_seq = 10
                    seq = 0
                    while ls(final_target) == 0 and seq < max_seq:
                        seq += 1
                        final_target = '-'.join((dst_dir, str(seq)))

                    if seq >= max_seq:
                        log.warning("Too many retries (>%d) to copy, removing job %d", max_seq, jedi_id)
                        del results[jedi_id]
                        continue

                    log.info("Final target dst dir: %s", final_target)

                    # create the directory
                    if mkdir(final_target) != 0:
                        log.warning("Could not create output dir %s, retrying later", final_target)
                        continue

                log.debug("Checking package %s for %s", package, str(jedi_id))

                status = self.__task_status(nightly_status, jedi_id)
                if status is not None:

                    # job_name = results[jedi_id][1]
                    # outfile = results[jedi_id][2]
                    index = results[jedi_id][3]

                    # skip single jobs if status is not final
                    # batch jobs do get checked every time so files which are ready are downloaded
                    if (index > 0) and (status not in final_states):
                        continue

                    # create executor if not already done
                    if executor is None:
                        log.info("Executor started with %d threads", self.max_jobs)
                        executor = concurrent.futures.ThreadPoolExecutor(max_workers=self.max_jobs)

                    unpack = configuration.get(self.nightly_release, self.project, self.platform, package, "unpack", False)
                    indexed_package = package + ('.' + str(index) if index > 0 else '')
                    log.info("Copy whatever ready from %s to %s using seq %d", indexed_package, dst, seq)
                    thread_id += 1
                    # new style copy
                    # copies.append(executor.submit(self.copy_job, thread_id, indexed_package, dst, user, unpack, tmp, seq))
                    # old copy style
                    copies.append(executor.submit(old_copy_job, self.art_directory, indexed_package, dst, unpack, tmp, seq))

                    # job in final state
                    if status in final_states:
                        # remove job from waiting queue
                        log.info("JediID %s finished with status %s", str(jedi_id), status)
                        del results[jedi_id]
                        log.info("Still waiting for results of %d jobs %s", len(results), results.keys())

                    log.info("Still waiting for results of %d jobs %s", len(results), results.keys())
        return copies

    def task_copies(self, copies):
        """Wait for copy jobs."""
        log = logging.getLogger(MODULE)
        if len(copies) <= 0:
            log.info("No need to wait for any copy jobs")
            return 0

        # wait for all copy jobs to finish
        number_of_copy_jobs = len(copies)
        log.info("Waiting for %d copy jobs to finish...", number_of_copy_jobs)
        for future in concurrent.futures.as_completed(copies):
            (indexed_package, exit_code, start_time, end_time) = future.result()
            if exit_code == 0:
                log.debug("Copied %s exit_code: %d", indexed_package, exit_code)
                log.debug("  starting %s until %s", start_time.strftime('%Y-%m-%dT%H:%M:%S'), end_time.strftime('%Y-%m-%dT%H:%M:%S'))
            else:
                log.error("Failed to copy: %s exit_code: %d", indexed_package, exit_code)
            number_of_copy_jobs -= 1
            log.info("Still waiting for %d copy jobs to finish...", number_of_copy_jobs)

        log.info("All copy jobs finished.")
        return 0

    def __nightly_status(self):
        """
        Get the status of all jobs running for this nightly release.

        Example of the url:
        https://bigpanda.cern.ch/tasks/?taskname=user.artprod.atlas.21.2.AnalysisBase.x86_64-centos7-gcc8-opt.2019-10-27T0347.*&json
        https://bigpanda.cern.ch/tasks/?json=true&taskname=user.artprod.atlas.21.2.AnalysisBase.x86_64-centos7-gcc8-opt.2019-10-27T0347.%2A

        Returns list of dictionaries like:

        [
          {
            "jeditaskid": 19573091,
            "taskname": "user.artprod.atlas.21.2.AnalysisBase.x86_64-centos7-gcc8-opt.2019-10-27T0347.1181424.SUSYTools/",
            "status": "failed",
            ...
          },
          {
            "jeditaskid": 19573090,
            "taskname": "user.artprod.atlas.21.2.AnalysisBase.x86_64-centos7-gcc8-opt.2019-10-27T0347.1181424.DirectIOART/",
            "status": "failed",
            "username": "artprod",
            ...
          },
          ...
        ]

        if failed returns empty list.
        """
        import requests
        import urllib3

        log = logging.getLogger(MODULE)

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        payload = {
            'json': 'true',
            'taskname': '.'.join(('user', 'artprod', 'atlas', self.nightly_release, self.project, self.platform, self.nightly_tag, '*'))
        }
        url = 'https://bigpanda.cern.ch/tasks/'

        try:
            r = requests.get(url, params=payload, verify=False)
            log.info('Requesting: %s', r.url)
            log.info('Requests status code: %s', str(r.status_code))
            if r.status_code == requests.codes.ok:
                s = r.json()
                if 'message' in s:
                    log.error("Error reply from server %s", s['message'])
                    log.fatal("Lost connection with %s", url)
                    exit(1)
                return r.json()
            return []
        except requests.exceptions.RequestException, e:
            log.error('%s for %s', e, r.url)
        except httplib.IncompleteRead, e:
            log.error('%s for %s', e, r.url)
        return []

    def __task_status(self, nightly_status, jedi_id):
        """
        Return status of job with given jedi_id (number).

        return 'done' if jedi_id = 0
        return None if jedi_id not found
        """
        log = logging.getLogger(MODULE)
        log.info('jedi_id: %s', jedi_id)

        # fake return for simulation
        if jedi_id <= 0:
            return "done"

        entry = search(nightly_status, 'jeditaskid', int(jedi_id))
        if entry is None:
            return None

        return entry['status'] if 'status' in entry else None

    def task_job(self, grid_options, sub_cmd, script_directory, sequence_tag, package, outfile, inform_panda, job_type='', number_of_tests=0, split=0, job_name='', inds=None, n_files=0, in_file=False, ncores=1, athena_mt=0, no_action=False):
        """
        Submit a batch or single job.

        Returns jedi_id or 0 if submission failed.
        """
        log = logging.getLogger(MODULE)

        # grid_options
        log.debug("Grid_options: %s", grid_options)
        # NOTE: practically sure that pathena does not forward the env into the grid.
        env = os.environ.copy()
        env['ART_GRID_OPTIONS'] = grid_options
        log.debug("ART_GRID_OPTIONS %s", env['ART_GRID_OPTIONS'])

        pathena_options = ' '.join(('--noBuild', '--expertOnly_skipScout', '--noEmail', '--maxAttempt', '2'))

        if sub_cmd == 'single':
            inds_option = ''
            if inds is not None:
                inds_option = ' '.join(('--inDS', inds))

            nfiles_option = ''
            nfiles_per_job_option = ''
            if inds is not None and n_files > 0:
                nfiles_option = ' '.join(('--nFiles', str(n_files)))
                nfiles_per_job_option = ' '.join(('--nFilesPerJob', str(n_files)))
                ncore_nfiles_per_job_option = ' '.join(('--nFilesPerJob', str(n_files)))

            large_job_option = ' '.join(('--long', '--memory', '4096'))
            split_command = ''
            if inds is not None and split > 0:
                split_command = ' '.join(('--split', str(split)))
                nfiles_per_job_option = ''
                large_job_option = ''

            in_file_option = ''
            if inds is not None and str(in_file):
                in_file_option = '--in=%IN'

            ncores_option = ''
            if athena_mt == 0 and ncores > 1:
                ncores_option = ' '.join(('--nCore', str(ncores)))
                nfiles_per_job_option = ncore_nfiles_per_job_option
                large_job_option = ''

            athena_mt_option = ''
            if ncores == 1 and athena_mt > 0:
                ncores_option = ' '.join(('--nCore', str(athena_mt)))
                large_job_option = ''

            # <script_directory> <sequence_tag> <package> <outfile> <job_name>
            internal_command = ' '.join(('grid', 'single'))
            pathena_type_options = ' '.join((large_job_option, inds_option, nfiles_option, nfiles_per_job_option, ncores_option, athena_mt_option))
            args = job_name

        else:
            # batch
            split_command = ' '.join(('--split', str(number_of_tests), '--nEventsPerJob', str(1)))
            in_file_option = ''

            # <script_directory> <sequence_tag> <package> <outfile> <job_type> <job_index>
            internal_command = ' '.join(('grid', 'batch'))
            args = ' '.join((job_type, '%FIRSTEVENT:0'))
            pathena_type_options = ''

        run_all_tests_option = '--run-all-tests' if self.run_all_tests else ''

        # PATH and PYTHONPATH are not available as such (forwarded) in the grid.
        # We therefore call the art-internal directly and it adds its own python path
        sub_command = ' '.join(('art/scripts/art-internal.py',
                                internal_command,
                                run_all_tests_option,
                                in_file_option,
                                script_directory,
                                sequence_tag,
                                package,
                                '%OUT.tar',
                                str(inform_panda),
                                args))

        cmd = ' '.join(('pathena',
                        grid_options,
                        pathena_options,
                        pathena_type_options,
                        '--trf',
                        '\"' + sub_command + '\"',
                        split_command,
                        '--outDS',
                        outfile,
                        '--extOutFile',
                        'art-job.json'))

        # --disableAutoRetry
        # --excludedSite=ANALY_TECHNION-HEP-CREAM
        # --site=ANALY_NIKHEF-ELPROD_SHORT,ANALY_NIKHEF-ELPROD"

        log.info("cmd: %s", cmd)

        # NOTE: for art-internal.py the art_script_directory can be used as it is copied there
        dir = os.path.join(self.submit_directory, package, 'run')

        jedi_id = -1
        if no_action:
            jedi_id = 0
        else:
            # run the command, 'no_action' is forwarded and used inside the script
            (exit_code, out, err, command, start_time, end_time, timed_out) = run_command(cmd, dir=dir, env=env)
            if exit_code != 0:
                log.error("pathena failed %d", exit_code)
                print(err)
            else:
                jedi_id = self.get_jedi_id(err)
            print(out)

        log.info('jedi_id: %s', str(jedi_id))
        return jedi_id

    def get_grid_options(self, package, config_file):
        """Return grid options for a package."""
        log = logging.getLogger(MODULE)
        if config_file is None:
            return ''

        configuration = ArtConfiguration(config_file)
        grid_options = configuration.get_option(self.nightly_release, self.project, self.platform, package, 'exclude-sites', '--excludedSite=')
        grid_options += ' ' + configuration.get_option(self.nightly_release, self.project, self.platform, package, 'sites', '--site=')
        log.info('grid_options: %s', grid_options)
        return grid_options

    def task(self, script_directory, package, job_type, sequence_tag, user, inform_panda, no_action=False, config_file=None):
        """
        Submit a task, consisting of multiple jobs.

        For 'single' jobs each task contains exactly one job.
        Returns a map of jedi_id to (package, test_name, out_file, seq)
        """
        log = logging.getLogger(MODULE)
        log.info('Running art task')

        grid_options = self.get_grid_options(package, config_file)

        test_directories = self.get_test_directories(self.get_script_directory())
        test_directory = test_directories[package]
        number_of_batch_tests = len(self.get_files(test_directory, job_type, "batch", self.nightly_release, self.project, self.platform, self.run_all_tests))

        outfile = self.rucio.get_outfile_name(user, package, sequence_tag, self.nightly_tag)

        result = {}

        # submit batch tests, index = 0
        if number_of_batch_tests > 0:
            self.exit_if_outfile_too_long(outfile)

            # Batch
            log.info("Batch")
            jedi_id = self.task_job(grid_options, "batch", script_directory, sequence_tag, package, outfile, inform_panda, job_type=job_type, number_of_tests=number_of_batch_tests, no_action=no_action)
            if jedi_id > 0:
                result[jedi_id] = (package, "", outfile, 0, None)

        # submit single tests, index > 1
        index = 1
        for job_name in self.get_files(test_directory, job_type, "single", self.nightly_release, self.project, self.platform, self.run_all_tests):
            job = os.path.join(test_directory, job_name)
            header = ArtHeader(job)
            inds = header.get(ArtHeader.ART_INPUT)
            n_files = header.get(ArtHeader.ART_INPUT_NFILES)
            split = header.get(ArtHeader.ART_INPUT_SPLIT)
            ncores = header.get(ArtHeader.ART_CORES)
            athena_mt = header.get(ArtHeader.ART_ATHENA_MT)

            outfile_test = self.rucio.get_outfile_name(user, package, sequence_tag, self.nightly_tag, str(index))
            self.exit_if_outfile_too_long(outfile_test)

            # Single
            log.info("Single")
            jedi_id = self.task_job(grid_options, "single", script_directory, sequence_tag, package, outfile_test, inform_panda, split=split, job_name=job_name, inds=inds, n_files=n_files, in_file=True, ncores=ncores, athena_mt=athena_mt, no_action=no_action)

            if jedi_id > 0:
                result[jedi_id] = (package, job_name, outfile_test, index, None)

            index += 1

        return result

    def batch(self, sequence_tag, package, out, inform_panda, job_type, job_index):
        """Run a single job by job_index of a 'batch' submission."""
        log = logging.getLogger(MODULE)
        log.info('Running art grid batch')
        log.info("%s %s %s %s %s %s %s %s %s", self.nightly_release, self.project, self.platform, self.nightly_tag, package, job_type, str(job_index), out, inform_panda)

        test_directories = self.get_test_directories(self.get_script_directory())
        test_directory = test_directories[package]

        test_list = self.get_files(test_directory, job_type, "batch", self.nightly_release, self.project, self.platform, self.run_all_tests)

        # NOTE: grid counts from 1
        index = int(job_index)
        job_name = test_list[index - 1]

        in_file = None

        return self.job(test_directory, package, job_name, job_type, out, inform_panda, in_file)

    def single(self, sequence_tag, package, out, inform_panda, job_name, in_file):
        """Run a single job by name of a 'single' submission."""
        log = logging.getLogger(MODULE)

        log.info('Running art grid single')
        log.info("%s %s %s %s %s %s %s %s %s", self.nightly_release, self.project, self.platform, self.nightly_tag, package, job_name, out, inform_panda, in_file)

        test_directories = self.get_test_directories(self.get_script_directory())
        test_directory = test_directories[package]

        job_type = 'grid'
        return self.job(test_directory, package, job_name, job_type, out, inform_panda, in_file)

    def job(self, test_directory, package, job_name, job_type, out, inform_panda, in_file):
        """Run a job."""
        log = logging.getLogger(MODULE)

        print('# ' + '=' * 78)
        print('\t\tART job name:\t ' + str(job_name))
        print('# ' + '=' * 78)

        log.info("art-job-name: %s", job_name)
        panda_id = os.getenv('PandaID', '0')
        if inform_panda == 'True':  # pragma: no cover
            # informing panda, ignoring errors for now
            self.inform_panda(panda_id, job_name, package, test_directory)

        test_file = os.path.join(test_directory, job_name)

        # Tests are called with arguments: PACKAGE TEST_NAME SCRIPT_DIRECTORY TYPE [IN_FILE]
        script_directory = self.get_script_directory()
        command = ' '.join((test_file, package, job_name, script_directory, job_type, in_file if in_file is not None else ''))

        log.debug(job_name)
        log.debug(test_directory)
        log.debug(command)

        # run the test
        env = os.environ.copy()

        # same as localSetupArt.sh
        env['PATH'] = ':'.join((self.art_directory, env['PATH']))
        env['PYTHONPATH'] = ':'.join((os.path.join(self.art_directory, '..', 'python'), env['PYTHONPATH']))

        # general info
        env['ArtScriptDirectory'] = script_directory
        env['ArtPackage'] = package
        env['ArtJobType'] = job_type
        env['ArtJobName'] = job_name
        if in_file is not None:
            env['ArtInFile'] = in_file

        header = ArtHeader(test_file)
        athena_mt = header.get(ArtHeader.ART_ATHENA_MT)
        ncores = header.get(ArtHeader.ART_CORES)
        if athena_mt == 0 and ncores > 1:
            nthreads = header.get(ArtHeader.ART_INPUT_NFILES)
            (exit_code, output, error, command, start_time, end_time, timed_out) = run_command_parallel(command, nthreads, ncores, env=env)
        else:
            (exit_code, output, error, command, start_time, end_time, timed_out) = run_command(command, env=env)
        print(output)
        if (exit_code != 0):
            log.error("Test %s failed %d", job_name, exit_code)
            print(error)
        # NOTE: exit_code always 0
        print(error)

        # gather results
        result = {}
        result['name'] = job_name
        result['exit_code'] = exit_code
        result['test_directory'] = test_directory
        result['result'] = ArtBase.get_art_results(output)
        result['panda_id'] = panda_id
        result['description'] = header.get(ArtHeader.ART_DESCRIPTION)

        # pick up potential art.yml
        yml_file = os.path.join(test_directory, 'art.yml')
        if os.path.isfile(yml_file):
            log.info("art.yml is found: %s", yml_file)
            with open(yml_file, 'r') as stream:
                try:
                    result.update(yaml.safe_load(stream))
                    log.info("JSON is updated with art.yml info")
                except yaml.YAMLError as exc:
                    log.warning(exc)

        # write out results
        with open(os.path.join(ArtRucio.ART_JOB), 'w') as jobfile:
            json.dump(result, jobfile, sort_keys=True, indent=4, ensure_ascii=False)
            log.info("Wrote %s", ArtRucio.ART_JOB)

        # grab the content of "jobReport.json", add the art dictionary and write it back
        if os.path.isfile(ArtGrid.JOB_REPORT):
            with open(ArtGrid.JOB_REPORT, 'r+') as json_file:
                info = json.load(json_file)
                info[ArtGrid.JOB_REPORT_ART_KEY] = result
                # write out results
                json_file.seek(0)
                json.dump(info, json_file, sort_keys=True, indent=4, ensure_ascii=False)
                json_file.truncate()
                log.info("Updated %s", ArtGrid.JOB_REPORT)
        else:
            with open(ArtGrid.JOB_REPORT, 'w') as json_file:
                info = {}
                info[ArtGrid.JOB_REPORT_ART_KEY] = result
                json.dump(info, json_file, sort_keys=True, indent=4, ensure_ascii=False)
                log.info("Updated %s", ArtGrid.JOB_REPORT)

        # pick up the outputs
        files = set()

        # pick up art-header named outputs
        for path_name in ArtHeader(test_file).get(ArtHeader.ART_OUTPUT):
            for out_name in glob.glob(path_name):
                files.add(out_name)

        # pick up explicitly named output files, if no names added by ArtHeader
        if not files:
            with open(test_file, "r") as f:
                for line in f:
                    line = uncomment_python(line) if os.path.splitext(test_file)[1] == '.py' else uncomment_sh(line)
                    out_names = re.findall(r"--output[^\s=]*[= ]*(\S*)", line)
                    log.debug(out_names)
                    for out_name in out_names:
                        out_name = out_name.strip('\'"')
                        if os.path.exists(out_name):
                            files.add(out_name)

        tar_file = tarfile.open(out, mode='w')
        for file in files:
            log.info('Tar file contains: %s', file)
            tar_file.add(file)

        tar_file.close()
        # Always return 0
        return 0

    def inform_panda(self, panda_id, job_name, package, test_directory, url="http://bigpanda.cern.ch/art/registerarttest/?json"):
        """Inform panda about the job we are running using panda ID."""
        log = logging.getLogger(MODULE)
        log.info('python : %s', os.system('which python'))
        log.info('python version : %s', os.system('python --version'))

        test_file = os.path.join(test_directory, job_name)
        header = ArtHeader(test_file)
        html = header.get(ArtHeader.ART_HTML)

        import requests

        n_attempts = 3
        timeout = 120

        payload = {}
        payload['pandaid'] = panda_id
        payload['testname'] = job_name
        payload['nightly_release_short'] = self.nightly_release_short
        payload['platform'] = self.platform
        payload['project'] = self.project
        payload['package'] = package
        payload['nightly_tag'] = self.nightly_tag
        if html is not None:
            payload['html'] = html

        headers = {'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'}

        for attempt in range(0, n_attempts):
            try:
                reply = requests.post(url, data=payload, headers=headers, timeout=timeout, verify=False)
                log.info('Attempt %d: Informed panda about %s %s %s', attempt, panda_id, job_name, package)
            except requests.RequestException as e:  # pragma: no cover
                log.warning('Attempt %d: Exception occured for %s %s %s %s', attempt, panda_id, job_name, package, e)
                continue

            if reply.status_code == 200:
                try:
                    reply = reply.json()
                except ValueError as e:  # pragma: no cover
                    log.error('Attempt %d: The panda inform response was corrupted for %s %s %s %s', attempt, panda_id, job_name, package, e)
                    continue
                if 'exit_code' in reply and reply['exit_code'] == 0:
                    return True

        log.error('Made %d attempts, Panda could not be informed about %s %s %s', n_attempts, panda_id, job_name, package)
        return False

    def list(self, package, job_type, index_type, json_format, user, out=None):
        """List all jobs available."""
        # make sure script directory exist
        self.exit_if_no_script_directory()

        json_array = []
        for entry in self.rucio.get_table(user, package, self.nightly_tag):
            # print entry
            json_array.append({
                'name': entry['job_name'],
                'grid_index': entry['grid_index'],
                'job_index': entry['job_index'],
                'single_index': entry['single_index'],
                'file_index': entry['file_index'],
                'outfile': entry['outfile']
            })

        file = sys.stdout if out is None else open(out, "w")

        if json_format:
            json.dump(json_array, file, sort_keys=True, indent=4)
            return 0

        i = 0
        print("Example FileName: user.artprod.atlas.21.0.Athena.x86_64-slc6-gcc62-opt.2018-02-25T2154.314889.TrigInDetValidation.<Single>", file=file)
        print("Example OutputName: user.artprod.<Job>.EXT1._<Grid>.tar.<File>", file=file)
        print(file=file)
        print('{:-^5}'.format('Index'),
              '{:-^60}'.format('Name'),
              '{:-^6}'.format('Grid'),
              '{:-^9}'.format('Job'),
              '{:-^6}'.format('Single'),
              '{:-^4}'.format('File'),
              '{:-^80}'.format('FileName'), file=file)

        for entry in json_array:
            print('{:5d}'.format(i),
                  '{:60}'.format('None' if entry['name'] is None else entry['name']),
                  '{:06d}'.format(entry['grid_index']),
                  '{:9d}'.format(entry['job_index']),
                  '{:6d}'.format(entry['single_index']),
                  '{:4d}'.format(entry['file_index']),
                  '{:80}'.format(entry['outfile']), file=file)
            i += 1

        return 0

    def log(self, package, test_name, user, out=None):
        """Print the log of a job."""
        log = logging.getLogger(MODULE)

        # make sure script directory exist
        self.exit_if_no_script_directory()

        tmp_tar = self.__get_tar(user, self.nightly_release, self.project, self.platform, self.nightly_tag, package, test_name, tar=False)
        if tmp_tar is None:
            log.error("No log tar file found")
            return 1

        tar = tarfile.open(tmp_tar)
        for name in tar.getnames():
            if ArtRucio.ATHENA_STDOUT in name:
                f = tar.extractfile(name)
                content = f.read()
                file = sys.stdout if out is None else open(out, "w")
                print(content, file=file)
                break
        tar.close()
        os.remove(tmp_tar)
        return 0

    def output(self, package, test_name, user):
        """Download the output of a job."""
        log = logging.getLogger(MODULE)

        # make sure script directory exist
        self.exit_if_no_script_directory()

        outfiles = self.rucio.get_outfiles(user, package, self.nightly_tag)
        if len(outfiles) == 0:
            log.error("No output file found")
            return 1

        outfile = outfiles[0]
        if not outfile.endswith(package):
            # remove .13
            outfile = os.path.splitext(outfile)[0]
        job_name = os.path.splitext(test_name)[0]
        tar_dir = os.path.join(tempfile.gettempdir(), outfile, job_name)
        mkdir(tar_dir)

        tmp_tar = self.__get_tar(user, self.nightly_release, self.project, self.platform, self.nightly_tag, package, test_name)
        if tmp_tar is None:
            log.error("No output tar file found")
            return 1

        tar = tarfile.open(tmp_tar)
        tar.extractall(path=tar_dir)
        tar.close()
        os.remove(tmp_tar)

        print("Output extracted in", tar_dir)

        return 0

    def download(self, package, test_name, max_refs, user, ref_dir=None, shell=False, max_tries=3, nightly_release=None, project=None, platform=None):
        """
        Download the output of a reference job.

        Returns ref_dir
        """
        log = logging.getLogger(MODULE)

        nightly_release = self.nightly_release if nightly_release is None else nightly_release
        project = self.project if project is None else project
        platform = self.platform if platform is None else platform

        previous_nightly_tags = self.__get_previous_nightly_tags(max_refs, nightly_release, project, platform, self.nightly_tag)
        log.info("Previous nightly tags to check: %s", previous_nightly_tags)

        previous_nightly_tag = None
        for tag in previous_nightly_tags:
            log.info("LOG Previous Nightly Tag: %s", str(tag))

            log.info("Use shell script to download = %s", shell)
            tmp_tar = self.__get_tar(user, nightly_release, project, platform, tag, package, test_name, shell=shell, max_tries=max_tries)
            if tmp_tar is None:
                log.error("No reference tar file found for tag = %s", tag)
            else:
                previous_nightly_tag = tag
                break

        if previous_nightly_tag is None:
            log.error("No previous nightly tag found for max_refs = %d", max_refs)
            return None

        if ref_dir is None:
            ref_dir = os.path.join('.', 'ref-' + previous_nightly_tag)
            mkdir(ref_dir)
        log.info("Ref Dir: %s", str(ref_dir))

        tar = tarfile.open(tmp_tar)
        for member in tar.getmembers():
            tar.extractall(path=ref_dir, members=[member])
        tar.close()
        os.remove(tmp_tar)

        return ref_dir

    def compare(self, package, test_name, max_refs, user, files, txt_files, diff_pool, diff_root, entries=-1, mode='detailed', order_trees=False, shell=False, out=None):
        """Compare current output against a job of certain days ago."""
        log = logging.getLogger(MODULE)

        ref_dir = self.download(package, test_name, max_refs, user, shell=shell)
        if ref_dir is None:
            log.error("No reference found for max_refs %d", max_refs)
        else:
            log.info("Reference comparison found for %s", ref_dir)
            return self.compare_ref('.', ref_dir, files, txt_files, diff_pool, diff_root, entries, mode, order_trees, out)

        log.error("No comparison done, no valid reference found")
        return 1

    def __get_tar(self, user, nightly_release, project, platform, nightly_tag, package, test_name, grid_index=-1, tmp=None, tar=True, shell=False, max_tries=3):
        """Open tar file for particular release."""
        log = logging.getLogger(MODULE)
        log.debug("Tar: %s", tar)
        tmp = tempfile.gettempdir() if tmp is None else tmp
        job_name = os.path.splitext(test_name)[0]

        wait_time = 5  # mins

        nightly_release_short = re.sub(r"-VAL-.*", "-VAL", nightly_release)
        rucio = ArtRucio(self.art_directory, nightly_release_short, project, platform)

        tries = max_tries
        while tries > 0:
            try:
                for entry in rucio.get_table(user, package, nightly_tag, shell, tmp):
                    # keep it for debugging
                    # print("E", entry, job_name, grid_index)
                    if entry['job_name'] == job_name and (grid_index < 0 or entry['grid_index'] == grid_index):

                        log.debug("index %d", entry['grid_index'])
                        rucio_name = self.__get_rucio_name(user, entry, 'tar' if tar else 'log')

                        log.debug("RUCIO: %s", rucio_name)

                        tmp_dir = tempfile.mkdtemp()
                        atexit.register(shutil.rmtree, tmp_dir, ignore_errors=True)

                        log.info("Use shell script to download = %s", shell)
                        log.info("Download %s", rucio_name)
                        exit_code = rucio.download(rucio_name, tmp_dir, shell)
                        if exit_code == 0:
                            tmp_tar = os.path.join(tmp_dir, 'user.' + user, rucio_name)
                            return tmp_tar

            except exceptions.Exception, e:
                log.warning('(Rucio) Exception: %s', e)
                # if e.errno != errno.ENOENT:
                tries -= 1
                if tries > 0:
                    log.info("Waiting %d mins", wait_time)
                    time.sleep(wait_time * 60)
                continue

            log.error("No log or tar found for package %s or test %s", package, test_name)
            return None

        log.error("Too many (%d) (Rucio) Exceptions", max_tries)
        return None

    def __get_rucio_name(self, user, entry, file_type):
        rucio_name = None
        if file_type == 'json':
            rucio_name = '.'.join(('user', user, str(entry['job_index']), 'EXT0', '_{0:06d}'.format(entry['grid_index']), 'art-job', 'json'))
        elif file_type == 'tar':
            rucio_name = '.'.join(('user', user, str(entry['job_index']), 'EXT1', '_{0:06d}'.format(entry['grid_index']), 'tar'))
        else:
            rucio_name = '.'.join((entry['outfile'], 'log', str(entry['job_index']), '{0:06d}'.format(entry['grid_index']), 'log.tgz'))

        if entry['file_index'] > 0:
            rucio_name = '.'.join((rucio_name, str(entry['file_index'])))

        return rucio_name

    def __get_previous_nightly_tags(self, max_refs, nightly_release, project, platform, nightly_tag, directory=CVMFS_DIRECTORY):
        """
        Return list of tags in reverse order for max_refs.

        If none found returns empty list.
        """
        result = []
        directory = os.path.join(directory, nightly_release)
        tags = os.listdir(directory)
        tags.sort(reverse=True)
        tags = [x for x in tags if re.match(r'\d{4}-\d{2}-\d{2}T\d{2}\d{2}', x)]
        for tag in tags:
            if tag < nightly_tag:
                # find if project and platform exists for this tag
                target_exists = len(glob.glob(os.path.join(directory, tag, project, '*', 'InstallArea', platform))) > 0
                if target_exists:
                    result.append(tag)
                    max_refs -= 1
            if max_refs == 0:
                break
        return result

    def createpoolfile(self):  # pragma: no cover
        """Create 'empty' poolfile catalog."""
        path = os.path.join('.', 'PoolFileCatalog.xml')
        with open(path, 'w+') as pool_file:
            pool_file.write('<!-- Edited By POOL -->\n')
            pool_file.write('<!DOCTYPE POOLFILECATALOG SYSTEM "InMemory">\n')
            pool_file.write('<POOLFILECATALOG>\n')
            pool_file.write('</POOLFILECATALOG>\n')

        return 0
