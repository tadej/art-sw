#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""Class to handle art-headers."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import logging
import re

from types import IntType
from types import ListType
from types import StringType

from art_misc import is_exe

MODULE = "art.header"


class ArtHeader(object):
    """Class to handle art-headers."""

    # headers in alphabetical order
    ART_ATHENA_MT = 'art-athena-mt'
    ART_CI = 'art-ci'
    ART_CORES = 'art-cores'
    ART_DESCRIPTION = 'art-description'
    ART_HTML = 'art-html'
    ART_INCLUDE = 'art-include'
    ART_INPUT = 'art-input'
    ART_INPUT_NFILES = 'art-input-nfiles'
    ART_INPUT_SPLIT = 'art-input-split'
    ART_OUTPUT = 'art-output'
    ART_RUNON = 'art-runon'
    ART_TYPE = 'art-type'

    def __init__(self, filename):
        """Keep arguments, setup patterns for re-use, define possible art-header definitions."""
        self.header_format = re.compile(r'#\s(art-[\w-]+):\s+(.+)$')
        self.header_format_error1 = re.compile(r'#(art-[\w-]*):\s*(.+)$')
        self.header_format_error2 = re.compile(r'#\s\s+(art-[\w-]*):\s*(.+)$')
        self.header_format_error3 = re.compile(r'#\s(art-[\w-]*):\S(.*)$')

        self.filename = filename

        self.header = {}

        self.valid = True

        # general
        self.add(ArtHeader.ART_DESCRIPTION, StringType, '')
        self.add(ArtHeader.ART_INCLUDE, ListType, [])        # not specifying results in inclusion
        self.add(ArtHeader.ART_RUNON, ListType, [])
        self.add(ArtHeader.ART_TYPE, StringType, None, ['build', 'grid'])

        # "build" type only
        self.add(ArtHeader.ART_CI, ListType, [])

        # "grid" type only
        self.add(ArtHeader.ART_ATHENA_MT, IntType, 0)
        self.add(ArtHeader.ART_CORES, IntType, 1)
        self.add(ArtHeader.ART_HTML, StringType, None)
        self.add(ArtHeader.ART_INPUT, StringType, None)
        self.add(ArtHeader.ART_INPUT_NFILES, IntType, 1)
        self.add(ArtHeader.ART_INPUT_SPLIT, IntType, 0)
        self.add(ArtHeader.ART_OUTPUT, ListType, [])

        self.__read(filename)

    def add(self, key, value_type, default_value=None, constraint=None, defined=True):
        """Add a single header definition."""
        self.header[key] = {}
        self.header[key]['type'] = value_type
        self.header[key]['default'] = default_value
        self.header[key]['constraint'] = constraint
        self.header[key]['defined'] = defined
        self.header[key]['value'] = None    # e.g. the value was never set
        self.header[key]['errors'] = []     # no errors

    def is_list(self, key):
        """Return true if key exists and is of ListType."""
        return self.header[key]['type'] is ListType if key in self.header else False

    def __read(self, filename):
        """Read all headers from file."""
        for line in open(filename, "r"):
            line_match = self.header_format.match(line)
            if line_match:
                key = line_match.group(1)
                value = line_match.group(2).strip()
                # convert value if needed
                try:
                    if key in self.header and self.header[key]['type'] == IntType:
                        value = int(value)
                except ValueError:
                    error = "Invalid value in art-header " + key + ": " + str(value) + " in file " + filename
                    self.header[key]['errors'].append(error)
                    self.valid = False

                if self.is_list(key):
                    # handle list types
                    if self.header[key]['value'] is None:
                        self.header[key]['value'] = []
                    self.header[key]['value'].append(value)
                else:
                    # handle values
                    if key not in self.header:
                        error = "Unknown art-header " + key + ": " + str(value) + " in file " + filename
                        self.add(key, StringType, defined=False)
                        self.header[key]['errors'].append(error)
                        self.valid = False
                    if self.header[key]['value'] is None:
                        self.header[key]['value'] = value
                    else:
                        error = "Key " + key + ": already set to '" + str(self.header[key]['value']) + "' in file " + filename
                        self.header[key]['errors'].append(error)
                        self.valid = False

                # further Validation: type incorrect and not None
                if type(self.header[key]['value']) != self.header[key]['type'] and not isinstance(self.header[key]['value'], type(None)):
                    error = "Type incorrect: " + str(type(self.header[key]['value'])) + " not valid for key: " + key + ", expected " + str(self.header[key]['type']) + " in file " + filename
                    self.header[key]['errors'].append(error)

                # further Validation: constraint not correct
                if self.header[key]['constraint'] is not None and self.header[key]['value'] not in self.header[key]['constraint']:
                    error = "Value: " + str(self.header[key]['value']) + " for key: " + key + " not in constraints: " + str(self.header[key]['constraint']) + " in file " + filename
                    self.header[key]['errors'].append(error)
                    self.valid = False

    def is_valid(self):
        """Return True if file was read without errors."""
        return self.valid

    def get(self, key):
        """
        Get the value of a header by key.

        Return default if header not specified.
        Warn and return None if header is not defined.
        """
        log = logging.getLogger(MODULE)
        if key not in self.header:
            log.warning("Art seems to look for a header key %s which is not in the list of defined headers: %s", key, self.header.keys())
            return None

        if self.header[key]['value'] is None:
            return self.header[key]['default']

        return self.header[key]['value']

    def validate(self):
        """
        Validate the '# art-*' headers in the file.

        Validation fails if:
        - test is not executable
        - a header is spaced correctly (e.g. '#art-header: value')
        - a value in a header is not spaced correctly (e.g. '# art-header:value')
        - errors were found during reading (validation in read)

        return True is valid
        """
        log = logging.getLogger(MODULE)
        log.info(self.filename)
        valid = True

        # important
        if not is_exe(self.filename):
            valid = False
            log.error("%s is NOT executable.", self.filename)

        # cosmetics
        for line in open(self.filename, "r"):
            if self.header_format_error1.match(line):
                valid = False
                log.error("LINE: %s", line.rstrip())
                log.error("Header Validation - invalid header format, use space between '# and art-xxx' in file %s", self.filename)
            if self.header_format_error2.match(line):
                valid = False
                log.error("LINE: %s", line.rstrip())
                log.error("Header Validation - invalid header format, too many spaces between '# and art-xxx' in file %s", self.filename)
            if self.header_format_error3.match(line):
                valid = False
                log.error("LINE: %s", line.rstrip())
                log.error("Header Validation - invalid header format, use at least one space between ': and value' in file %s", self.filename)

        # report errors found during __read
        for key in self.header:
            for error in self.header[key]['errors']:
                valid = False
                log.error(error)

        return valid
