#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""Miscellaneous functions."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import concurrent.futures
import glob
import gzip
import logging
import os
import re
import shlex
import shutil
import signal
import subprocess
import sys
import threading

from datetime import datetime

MODULE = "art.misc"
EOS_MGM_URL = 'root://eosatlas.cern.ch/'
CVMFS_DIRECTORY = '/cvmfs/atlas-nightlies.cern.ch/repo/sw'

KByte = 1024
MByte = KByte * 1024
GByte = MByte * 1024


def set_log(kwargs):
    """Set the default log level and message format depending on --verbose or --quiet options."""
    level = logging.DEBUG if 'verbose' in kwargs and kwargs['verbose'] else logging.WARN if 'quiet' in kwargs and kwargs['quiet'] else logging.INFO
    set_log_level(level)


def set_log_level(level):
    """Set the default log level and message format."""
    log = logging.getLogger("art")
    log.setLevel(level)
    threading.current_thread().name = str(0).zfill(2)

    if not len(log.handlers):
        # create and attach new handler, disable propagation to root logger to avoid double messages
        handler = logging.StreamHandler(sys.stdout)
        format_string = "%(asctime)s %(name)15s.%(funcName)-15s %(levelname)8s T%(threadName)s %(message)s"
        date_format_string = None
        formatter = logging.Formatter(format_string, date_format_string)
        handler.setFormatter(formatter)
        log.addHandler(handler)
        log.propagate = False


def get_atlas_env():
    """Get all environment variables."""
    log = logging.getLogger(MODULE)
    try:
        nightly_release = os.environ['AtlasBuildBranch']
        project = os.environ['AtlasProject']
        platform = os.environ[project + '_PLATFORM']
        nightly_tag = os.environ['AtlasBuildStamp']
        log.debug("NIGHTLY_RELEASE = %s", nightly_release)
        log.debug("PROJECT         = %s", project)
        log.debug("PLATFORM        = %s", platform)
        log.debug("NIGHTLY_TAG     = %s", nightly_tag)
        return (nightly_release, project, platform, nightly_tag)
    except KeyError, e:
        log.critical("Environment variable not set %s", e)
        sys.exit(1)


def build_script_directory(script_directory, nightly_release, project, platform, nightly_tag='latest'):
    """Return calculated script directory, sometimes overriden by commandline."""
    # returns something like: /cvmfs/atlas-nightlies.cern.ch/repo/sw/21.0/2018-12-08T2252/Athena/21.0.91/InstallArea/x86_64-slc6-gcc62-dbg/src
    log = logging.getLogger(MODULE)
    if script_directory is not None:
        return script_directory

    base_directory = CVMFS_DIRECTORY
    if not os.path.isdir(base_directory):  # pragma: no cover
        log.critical("No access to cvmfs directory")
        exit(1)

    base_directory = os.path.join(base_directory, nightly_release)  # e.g. 21.0
    log.debug("Listing %s", base_directory)
    nightly_tags = [nightly_tag] if nightly_tag != 'latest' else sorted(glob.glob(os.path.join(base_directory, '????-??-??T????')), reverse=True)
    for nightly_tag in nightly_tags:
        script_directory = os.path.join(base_directory, nightly_tag)  # e.g. 2017-10-25T2150
        script_directory = os.path.join(script_directory, project)  # e.g. Athena
        try:
            script_directory = os.path.join(script_directory, os.listdir(script_directory)[0])  # e.g. 21.0.3
            script_directory = os.path.join(script_directory, os.listdir(script_directory)[0])  # InstallArea
        except OSError:  # pragma: no cover
            script_directory = os.path.join(script_directory, '*', '*')
        script_directory = os.path.join(script_directory, platform)  # x86_64-slc6-gcc62-opt
        script_directory = os.path.join(script_directory, 'src')    # src
        log.debug("Checking %s", script_directory)
        if os.path.isdir(script_directory):
            log.debug("SCRIPT_DIRECTORY = %s", script_directory)
            return script_directory
    return None


def get_release(script_directory):
    """Return (nightly_release, project, platform, nighly_tag), or (None, None, None, None)."""
    # script_directory /cvmfs/atlas-nightlies.cern.ch/repo/sw/21.0/2018-12-08T2252/Athena/21.0.91/InstallArea/x86_64-slc6-gcc62-dbg/src
    regexp = r"/(?P<nightly_release>[^/]*)/(?P<nighly_tag>\d{4}-\d{2}-\d{2}T\d{4})/(?P<project>[^/]*)/[^/]*/[^/]*/(?P<platform>[^/]*)/"
    match = re.search(regexp, script_directory)
    if match:
        return (match.group('nightly_release'), match.group('project'), match.group('platform'), match.group('nighly_tag'))
    return (None, None, None, None)


def split_release(release, nightly_release=None, project=None, platform=None):
    """Return (nightly_release, project, platform), defaults can be given."""
    # 21.0/Athena/x86_64-slc6-gcc62-dbg
    separator = '/'
    count = release.count(separator)
    if count >= 2:
        (nightly_release, project, platform) = release.split(separator, 3)
    elif count == 1:
        (nightly_release, project) = release.split(separator, 2)
    elif release != "":
        nightly_release = release

    return (nightly_release, project, platform)


# do not use named arguments, its called by Timer
def interrupt_command(proc, name='', timed_out=None, verbose=True):
    """Kill a process after timeout."""
    if timed_out is not None:
        timed_out["value"] = True
    if verbose:
        print "Command interrupted due to timeout", proc.pid, name
    try:
        os.killpg(os.getpgid(proc.pid), signal.SIGKILL)
    except OSError as e:  # pragma: no cover
        print "_kill_proc.killpg(): Could not kill", proc, e
    try:
        proc.kill()
    except OSError as e:  # pragma: no cover
        print "_kill_proc.kill(): Could not kill", proc, e
    try:
        proc.terminate()
    except OSError as e:  # pragma: no cover
        print "_kill_proc.terminate(): Could not kill", proc, e


def run_command(cmd, dir=None, shell=False, env=None, timeout=0, proc=None, verbose=True):
    """
    Run the given command locally.

    The command runs as separate subprocesses for every piped command.
    Returns tuple (exit_code, output, err, cmd, start_time, end_time, timed_out)
    """
    # leave at print for basic debugging, log sometimes lost
    start_time = datetime.now()
    if "|" in cmd:
        cmd_parts = cmd.split('|')
    else:
        cmd_parts = []
        cmd_parts.append(cmd)
    i = 0
    p = {}
    for cmd_part in cmd_parts:
        cmd_part = cmd_part.strip()
        if i == 0:
            p[i] = subprocess.Popen(shlex.split(cmd_part), stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=dir, shell=shell, env=env, preexec_fn=os.setpgrp)
        else:
            p[i] = subprocess.Popen(shlex.split(cmd_part), stdin=p[i - 1].stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=dir, shell=shell, env=env)
        i = i + 1

    # Max process index
    n = i - 1
    if verbose:
        print "Max process index:", n

    # if verbose:
    if verbose:
        print "Execute:", p[n].pid, cmd

    # set the proc value for interruption
    if proc is not None:
        proc['value'] = p[n]

    # start timer for timeout
    timer = None
    timed_out = {"value": False}
    if timeout > 0:
        timer = threading.Timer(timeout, interrupt_command, [p[n], cmd, timed_out, verbose])
        timer.start()

    # run command(s)
    if verbose:
        print "Waiting for:", n, p[n].pid, cmd
    (output, err) = p[n].communicate()
    if verbose:
        print "Finished waiting for:", p[n].pid, "exit code", p[n].returncode

    # cancel timer
    if timer:
        timer.cancel()
        if verbose:
            print "Cancelled Timer", p[n].pid

    # pick up errors
    for k in reversed(range(0, n)):
        try:
            (cmd_output, cmd_err) = p[k].communicate()
            err = cmd_err + err
        except ValueError:  # pragma: no cover
            pass

    # note if timed out
    if timed_out["value"]:
        exit_code = 1
        err = str(err) + '\nProcess timed out after ' + str(timeout) + ' seconds.\n'
        if verbose:
            print "Process timed out", p[n].pid, p[n].returncode
    else:
        if verbose:
            print "Waiting for exit code:", p[n].pid
        exit_code = p[0].wait()

    if verbose:
        for k in range(0, n + 1):
            print "Process Exit Codes", k, p[k].returncode
        print "Exit Code:", p[n].pid, exit_code

    end_time = datetime.now()

    return (exit_code, str(output), str(err), cmd, start_time, end_time, timed_out["value"])


def run_command_parallel(cmd, nthreads, ncores, dir=None, shell=False, env=None, timeout=0, verbose=True):
    """
    Run the given command locally in parallel.

    The command runs as separate subprocesses for every piped command.
    Returns tuple of exit_code, output and err.
    """
    start_time = datetime.now()
    log = logging.getLogger(MODULE)
    ncores = min(ncores, nthreads)

    if env is None:
        env = os.environ.copy()

    env['ArtThreads'] = str(nthreads)
    env['ArtCores'] = str(ncores)

    # Results
    full_exit_code = 0
    full_out = ''
    full_err = ''

    # Start
    env['ArtProcess'] = "start"
    (exit_code, out, err, command, start_time_start, end_time_start, timed_out) = run_command(cmd, dir=dir, shell=shell, env=env, timeout=timeout, verbose=verbose)
    full_exit_code = full_exit_code if exit_code == 0 else exit_code
    if verbose:
        full_out += "-+-art-process start out " + start_time_start.strftime('%Y-%m-%dT%H:%M:%S') + "\n"
    full_out += out
    if verbose:
        full_out += "---art-process start out " + end_time_start.strftime('%Y-%m-%dT%H:%M:%S') + "\n"
        full_err += "-+-art-process start err " + start_time_start.strftime('%Y-%m-%dT%H:%M:%S') + "\n"
    full_err += err
    if verbose:
        full_err += "---art-process start err " + end_time_start.strftime('%Y-%m-%dT%H:%M:%S') + "\n"

    if not timed_out:
        log.info("Creating executor with cores: %d", ncores)
        executor = concurrent.futures.ThreadPoolExecutor(ncores)
        future_set = []

        # Processing
        log.info("Running threads: %d", nthreads)
        for index in range(nthreads):
            process_env = env.copy()
            process_env['ArtProcess'] = str(index)
            future_set.append(executor.submit(run_command, cmd, dir=dir, shell=shell, env=process_env, timeout=timeout, verbose=verbose))

        log.info("Waiting for threads to finish...")
        concurrent.futures.wait(future_set)
        for index, future in enumerate(future_set):
            (exit_code, out, err, command, start_time_process, end_time_process, para_timed_out) = future.result()
            timed_out |= para_timed_out
            full_exit_code = full_exit_code if exit_code == 0 else exit_code
            if verbose:
                full_out += "-+-art-process " + str(index) + " out " + start_time_process.strftime('%Y-%m-%dT%H:%M:%S') + "\n"
            full_out += out
            if verbose:
                full_out += "---art-process " + str(index) + " out " + end_time_process.strftime('%Y-%m-%dT%H:%M:%S') + "\n"
                full_err += "-+-art-process " + str(index) + " err " + start_time_process.strftime('%Y-%m-%dT%H:%M:%S') + "\n"
            full_err += err
            if verbose:
                full_err += "---art-process " + str(index) + " err " + end_time_process.strftime('%Y-%m-%dT%H:%M:%S') + "\n"

        if not timed_out:
            # End
            env['ArtProcess'] = "end"
            (exit_code, out, err, command, start_time_end, end_time_end, timed_out) = run_command(cmd, dir=dir, shell=shell, env=env, timeout=timeout, verbose=verbose)
            full_exit_code = full_exit_code if exit_code == 0 else exit_code
            if verbose:
                full_out += "-+-art-process end out " + start_time_end.strftime('%Y-%m-%dT%H:%M:%S') + "\n"
            full_out += out
            if verbose:
                full_out += "---art-process end out " + end_time_end.strftime('%Y-%m-%dT%H:%M:%S') + "\n"
                full_err += "-+-art-process end err " + start_time_end.strftime('%Y-%m-%dT%H:%M:%S') + "\n"
            full_err += err
            if verbose:
                full_err += "---art-process end err " + end_time_end.strftime('%Y-%m-%dT%H:%M:%S') + "\n"

    end_time = datetime.now()

    return (full_exit_code, full_out, full_err, cmd, start_time, end_time, timed_out)


def is_exe(path):
    """Return True if path is executable."""
    return os.path.isfile(path) and os.access(path, os.X_OK)


def make_executable(path):
    """Make file executable (chmod +x)."""
    mode = os.stat(path).st_mode
    mode |= (mode & 0o444) >> 2    # copy R bits to X
    os.chmod(path, mode)


def mkdir(path):
    """Make (missing) directories."""
    log = logging.getLogger(MODULE)

    if os.path.isfile(path):
        log.error("mkdir path is file %s", path)
        return 1

    if os.path.isdir(path):
        return 0

    try:
        os.makedirs(path)
    except OSError as e:  # pragma: no cover
        log.info("mkdir error ignored: %s", e)
        pass

    return 0


def ls(path):
    """List files in directory."""
    if path.startswith('/eos'):
        ls_cmd = 'eos ' + EOS_MGM_URL + ' ls ' + path + '/'
    else:
        ls_cmd = 'ls ' + path + '/'

    (exit_code, out, err, command, start_time, end_time, timed_out) = run_command(ls_cmd, verbose=False)
    if exit_code == 0:
        print out
        print err

    return exit_code


def cat(path):
    """Cat file."""
    if path.startswith('/eos'):
        ls_cmd = 'eos ' + EOS_MGM_URL + ' cat ' + path
    else:
        ls_cmd = 'cat ' + path

    (exit_code, out, err, command, start_time, end_time, timed_out) = run_command(ls_cmd, verbose=False)
    if exit_code == 0:
        print out
        print err

    return exit_code


art_xrdcp_version = False


def xrdcp(src, dst):
    """Copy files to directory."""
    global art_xrdcp_version
    log = logging.getLogger(MODULE)

    if not art_xrdcp_version:
        art_xrdcp_version = True
        # check which xrdcp we are running
        (exit_code, out, err, command, start_time, end_time, timed_out) = run_command('which xrdcp')
        [log.info('which: ' + line) for line in out.splitlines()]
        [log.error('which: ' + line) for line in err.splitlines()]

        # check which version of xrdcp we are running
        (exit_code, out, err, command, start_time, end_time, timed_out) = run_command('xrdcp --version')
        [log.info('version: ' + line) for line in out.splitlines()]
        [log.error('version: ' + line) for line in err.splitlines()]

    if dst.startswith('/eos'):  # pragma: no cover
        dst = EOS_MGM_URL + dst + '/'
    else:
        if not os.path.isfile(dst) and not os.path.isdir(dst):
            log.info("Creating dirs: %s", dst)
            os.makedirs(dst)
        dst = dst + '/'

    # run the actual command
    (exit_code, exit_out, exit_err, command, start_time, end_time, timed_out) = __xrdcp(src, dst, force=True, recursive=True, verbose=True, path=True)

    if exit_code != 0:  # pragma: no cover
        log.error("COPY to DST Error: %d", exit_code)
        [log.info('copy: ' + line) for line in exit_out.splitlines()]
        [log.error('copy: ' + line) for line in exit_err.splitlines()]

    return exit_code


def __xrdcp(src, dst, force=False, recursive=False, path=False, verbose=False):
    """Copy using xrdcp."""
    if src is None or dst is None:
        return (1, "", "src or dst None", "__xrdcp", None, None, False)  # pragma: no cover
    log = logging.getLogger(MODULE)
    cmd = ' '.join(('xrdcp --nopbar', '--force' if force else '', '--recursive' if recursive else '', '--path' if path else '', '--verbose' if verbose else '', src, dst))
    log.info("Using: %s", cmd)
    return run_command(cmd, verbose=False)


def count_files(path):
    """Count number of files."""
    log = logging.getLogger(MODULE)

    if path.startswith('/eos'):  # pragma: no cover
        cmd = ' '.join(('eos', EOS_MGM_URL, 'find', path, '|', 'wc', '-l'))
    else:
        cmd = ' '.join(('find', path, '|', 'wc', '-l'))

    (exit_code, out, err, command, start_time, end_time, timed_out) = run_command(cmd, verbose=False)
    if exit_code == 0:
        nFiles = int(out)
        return nFiles

    log.error("Error retrieving number of files on %s, %s", path, err)  # pragma: no cover
    return -1  # pragma: no cover


def touch(fname, times=None):
    """Touch a file."""
    with open(fname, 'a'):
        os.utime(fname, times)


def rm(fname):
    """Remove a file."""
    try:
        os.remove(fname)
    except OSError:
        pass


def which(program):
    """Show which program is actually found on the PATH."""
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None


def memory(scale=1):
    """Return free memory."""
    return os.sysconf('SC_PHYS_PAGES') * os.sysconf('SC_PAGE_SIZE') / scale


def dict_added(current, ref):
    """Return keys added."""
    current_keys = set(current.keys())
    ref_keys = set(ref.keys())
    both_keys = current_keys.intersection(ref_keys)
    return current_keys - both_keys


def dict_removed(current, ref):
    """Return keys removed."""
    current_keys = set(current.keys())
    ref_keys = set(ref.keys())
    both_keys = current_keys.intersection(ref_keys)
    return ref_keys - both_keys


def dict_changed(current, ref):
    """Return keys in both for which values have changed."""
    current_keys = set(current.keys())
    ref_keys = set(ref.keys())
    both_keys = current_keys.intersection(ref_keys)
    return set(o for o in both_keys if ref[o] != current[o])


def find(path):
    """Similar to unix find, just to show all the paths."""
    log = logging.getLogger(MODULE)

    for dpath, dnames, fnames in os.walk(path):
        for i, fname in enumerate([os.path.join(dpath, fname) for fname in fnames]):
            log.info("%s", fname)


def gunzip(file_path, output_path):
    """Gunzip a file."""
    with gzip.open(file_path, "rb") as f_in:
        with open(output_path, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)


def uncomment_sh(line):
    """Uncomment shell line taking care of quotes."""
    parts = shlex.split(line, comments=True)
    line = ' '.join(parts)
    return line


def uncomment_python(line):
    """Uncomment python line taking care of quotes."""
    # a little state machine with two state varaibles:
    in_quote = False  # whether we are in a quoted string right now
    backslash_escape = False  # true if we just saw a backslash

    for i, ch in enumerate(line):
        if not in_quote and ch == '#':
            # not in a quote, saw a '#', it's a comment.  Chop it and return!
            return line[:i]
        elif backslash_escape:
            # we must have just seen a backslash; reset that flag and continue
            backslash_escape = False
        elif in_quote and ch == '\\':
            # we are in a quote and we see a backslash; escape next char
            backslash_escape = True
        elif ch == '"':
            in_quote = not in_quote

    return line


def search(list, key, value, default=None):
    """Return first dictionary with given key, value in array of dicts."""
    for item in list:
        if item[key] == value:
            return item
    return default
