#!/bin/sh
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
# author : Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>

set -e

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh || echo $? && true
NIGHTLY_RELEASE=$4
NIGHTLY_RELEASE_SHORT=${NIGHTLY_RELEASE/-VAL-*/-VAL}
export RUCIO_ACCOUNT=artprod
lsetup panda || echo $? && true
asetup --platform=$6 ${NIGHTLY_RELEASE_SHORT},$7,$5 || echo $? && true
lsetup rucio -a artprod || echo $? && true
voms-proxy-init --rfc -noregen -cert ./grid.proxy -voms atlas

lsetup -f "xrootd 4.8.4" || echo $? && true
which xrdcp

source share/localSetupART.sh
source test/localTestSetup.sh

coverage run --parallel-mode -m unittest -v $1
xrdcp -f -N .coverage.* root://eosuser.cern.ch/$3/.coverage.$1
coverage combine
## coverage report -m | grep $2
