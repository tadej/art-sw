# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
image: gitlab-registry.cern.ch/art/art-gitlab-ci-runner:cc7

variables:
  NIGHTLY_RELEASE: "21.0"
  PROJECT: "Athena"
  PLATFORM: "x86_64-slc6-gcc62-opt"
  NIGHTLY_TAG: "latest"
  PACKAGE: "Tier0ChainTests"
  PACKAGES: "Tier0ChainTests"
  SSH_OPTIONS: -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPIDelegateCredentials=yes -o GSSAPITrustDNS=yes
  LOGIN: ${KRB_USERNAME}@lxplus.cern.ch
  EOSPROJECT: root://eosuser.cern.ch
  DST: /eos/user/a/artprod
  COV: ${DST}/coverage_files/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
  WWW: ${DST}/www

before_script:
  - export EOS_MGM_URL=${EOSPROJECT}
  - export KRB5CCNAME=/tmp/krb5cc_0_${CI_JOB_NAME}_${CI_JOB_ID}
  - echo "${KRB5CCNAME} ${KRB_USERNAME}@CERN.CH"
  # echo "${KRB_PASSWORD}" | kinit -V -c /tmp/krb5cc_0_${CI_JOB_NAME}_${CI_JOB_ID} ${KRB_USERNAME}@CERN.CH
  # klist -c /tmp/krb5cc_0_${CI_JOB_NAME}_${CI_JOB_ID}
  - echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH
  - klist

stages:
  - checks
  - unittest
  - atlastest
  - gridtest
  - report

.common: &common
  tags:
  - docker
  - cvmfs
  - art-runner

report:
  <<: [ *common ]
  stage: report
  script:
  - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  - source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh || echo $? && true
  - NIGHTLY_RELEASE_SHORT=${NIGHTLY_RELEASE/-VAL-*/-VAL}
  - asetup --platform=${PLATFORM} ${NIGHTLY_RELEASE_SHORT},${NIGHTLY_TAG},${PROJECT} || echo $? && true
  - which xrdcp
  # 4.8.4 recursive copy fails with Error indexing remote directory
  - lsetup -f "xrootd 4.7.1" || echo $? && true
  - which xrdcp
  - xrdcp -R -f -N ${EOSPROJECT}/${COV} .
  - coverage combine
  - coverage report -m | grep TOTAL
  # eos mkdir does not work!!!
  - mkdir -p coverage/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
  - coverage html -d coverage/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
  - cd coverage
  - xrdcp -r -f -N . ${EOSPROJECT}/${WWW}/coverage

tar:
  <<: [ *common ]
  stage: report
  script:
  - tar -zcvf art-${CI_COMMIT_REF_NAME}.tar.gz -X ignoreForTar.txt *
  artifacts:
    paths:
    - art-${CI_COMMIT_REF_NAME}.tar.gz
  when: manual

config:
  <<: [ *common ]
  stage: checks
  script:
  - echo ${NIGHTLY_RELEASE}
  - echo ${PROJECT}
  - echo ${PLATFORM}
  - echo ${NIGHTLY_TAG}
  - echo ${PACKAGE}
  - echo ${PACKAGES}

shellcheck:
  <<: [ *common ]
  stage: checks
  script:
  - shellcheck --version
  - shellcheck scripts/art-download.sh
  - shellcheck scripts/art-task-build.sh
  # - shellcheck scripts/art-rucio-download.sh
  - shellcheck scripts/art-rucio-list-dids.sh
  - shellcheck share/localSetupART.sh
  # shellcheck share/localSetupART.zsh

python-flake8:
  <<: [ *common ]
  stage: checks
  script:
  - python --version
  - python -m flake8 --ignore=E501

test-art-base:
  <<: [ *common ]
  stage: unittest
  script:
  # execute .py pickup rather than .pyc in ArtTestCase
  - python -m py_compile python/ART/art_testcase.py
  - ./ci-test-simple.sh test_art_base art_base ${COV}

test-art-build:
  <<: [ *common ]
  stage: atlastest
  script:
  - echo "$PRIVATE_GRID_PROXY" > grid.proxy
  - chmod 600 grid.proxy
  - ./ci-test-atlas.sh test_art_build art_build ${COV} ${NIGHTLY_RELEASE} ${PROJECT} ${PLATFORM} ${NIGHTLY_TAG}

test-art-configuration:
  <<: [ *common ]
  stage: unittest
  script:
  - ./ci-test-simple.sh test_art_configuration art_configuration ${COV}

.test-art-diff:
  <<: [ *common ]
  stage: atlastest
  script:
  - echo "$PRIVATE_GRID_PROXY" > grid.proxy
  - chmod 600 grid.proxy
  - ./ci-test-atlas.sh test_art_diff scripts/art-diff ${COV} ${NIGHTLY_RELEASE} ${PROJECT} ${PLATFORM} ${NIGHTLY_TAG}

test-art-grid:
  <<: [ *common ]
  stage: unittest
  script:
  - ./ci-test-simple.sh test_art_grid art_grid ${COV}

test-art-header:
  <<: [ *common ]
  stage: unittest
  script:
  - ./ci-test-simple.sh test_art_header art_header ${COV}

test-art-misc:
  <<: [ *common ]
  stage: unittest
  script:
  - ./ci-test-simple.sh test_art_misc art_misc ${COV}

test-art-rucio:
  <<: [ *common ]
  stage: atlastest
  script:
  - echo "$PRIVATE_GRID_PROXY" > grid.proxy
  - chmod 600 grid.proxy
  - ./ci-test-atlas.sh test_art_rucio art_rucio ${COV} ${NIGHTLY_RELEASE} ${PROJECT} ${PLATFORM} ${NIGHTLY_TAG}

.test-art-script:
  <<: [ *common ]
  stage: atlastest
  script:
  - echo "$PRIVATE_GRID_PROXY" > grid.proxy
  - chmod 600 grid.proxy
  # execute .py pickup rather than .pyc in art.py
  - python -m py_compile scripts/art.py
  - ./ci-test-atlas.sh test_art_script scripts/art ${COV} ${NIGHTLY_RELEASE} ${PROJECT} ${PLATFORM} ${NIGHTLY_TAG}

test-art-submit:
  <<: [ *common ]
  stage: gridtest
  script:
  - echo "$PRIVATE_GRID_PROXY" > grid.proxy
  - chmod 600 grid.proxy
  - ./ci-test-atlas.sh test_art_submit scripts/art ${COV} ${NIGHTLY_RELEASE} ${PROJECT} ${PLATFORM} ${NIGHTLY_TAG}
  only:
  - tags
  - schedules

test-art-submit-debug:
  <<: [ *common ]
  stage: gridtest
  script:
  - echo "$PRIVATE_GRID_PROXY" > grid.proxy
  - chmod 600 grid.proxy
  - ./ci-test-atlas.sh test_art_submit scripts/art ${COV} ${NIGHTLY_RELEASE} ${PROJECT} ${PLATFORM} ${NIGHTLY_TAG}
  only:
    variables:
      - $CI_COMMIT_MESSAGE =~ /grid-debug/

test-art-trigger:
  <<: [ *common ]
  stage: unittest
  script:
  - ./ci-test-simple.sh test_art_trigger scripts/art-trigger ${COV}

test-art-xrdcp:
  <<: [ *common ]
  stage: atlastest
  script:
  - echo "$PRIVATE_GRID_PROXY" > grid.proxy
  - chmod 600 grid.proxy
  - ./ci-test-atlas.sh test_art_xrdcp art_xrdcp ${COV} ${NIGHTLY_RELEASE} ${PROJECT} ${PLATFORM} ${NIGHTLY_TAG}
