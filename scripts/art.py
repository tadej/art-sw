#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""
ART - ATLAS Release Tester.

You need to setup for an ATLAS release before using ART.

Usage:
  art.py run             [-v -q --type=<T> --max-jobs=<N> --ci --run-all-tests --timeout=<S> --copy=<dir>] <script_directory> <sequence_tag> [<test_names>...]
  art.py grid            [-v -q --type=<T> --max-jobs=<N> -n --run-all-tests] <script_directory> <sequence_tag>
  art.py submit          [-v -q --type=<T> --max-jobs=<N> --config=<file> -n --run-all-tests] <sequence_tag> [<packages>...]
  art.py copy            [-v -q --user=<user> --dst=<dir> --unpack --tmp=<dir> --seq=<N> --keep-tmp] <indexed_package>
  art.py validate        [-v -q] [<script_directory>]
  art.py included        [-v -q --type=<T> --test-type=<TT> --out=<file>] [<script_directory>]
  art.py download        [-v -q --max-refs=<N> --user=<user> --dst=<dir> --nightly-release=<release> --project=<project> --platform=<platform>] <package> <test_name>
  art.py compare grid    [-v -q --max-refs=<N> --user=<user> --entries=<entries> --file=<pattern>... --txt-file=<file>... --mode=<mode> --diff-pool --diff-root --out=<file> --order-trees] <package> <test_name>
  art.py compare ref     [-v -q --entries=<entries> --file=<pattern>... --txt-file=<file>... --mode=<mode> --diff-pool --diff-root --out=<file> --order-trees] <path> <ref_path>
  art.py list grid       [-v -q --user=<user> --json --test-type=<TT> --out=<file>] <package>
  art.py log grid        [-v -q --user=<user> --out=<file>] <package> <test_name>
  art.py output grid     [-v -q --user=<user>] <package> <test_name>
  art.py config          [-v -q --config=<file>] [<package>]
  art.py createpoolfile  [-v -q]
  art.py input-collect   [-v -q --type=<T> --run-all-tests --copy=<dir>] <release>
  art.py input-merge     [-v -q --work-dir=<dir> --ref-file=<file>]

Options:
  --ci                                  Run Continuous Integration tests only (using env: AtlasBuildBranch)
  --config=<file>                       Use specific config file [default: art-configuration.yml]
  --copy=<dir>                          Copy results to <dir>
  --diff-pool                           Do comparison with POOL
  --diff-root                           Do comparison with ROOT (none specified means POOL and ROOT are compared)
  --dst=<dir>                           Destination directory for downloaded files
  --entries=<entries>                   Number of entries to compare [default: 10]
  --file=<pattern>...                   Compare the following file patterns for diff-root [default: *AOD*.pool.root *ESD*.pool.root *HITS*.pool.root *RDO*.pool.root *TAG*.root]
  -h --help                             Show this screen
  --json                                Output in json format
  --keep-tmp                            Keep temporary directory while copying
  --max-jobs=<N>                        Maximum number of concurrent jobs to run [default: 0]
  --max-refs=<N>                        Maximum number of references to look for valid reference for compare or download [default: 7]
  --mode=<mode>                         Sets the mode for diff-root {summary, detailed} [default: detailed]
  --nightly-release=<nighly_release>    Use different nighly_release for download
  -n --no-action                        No real submit will be done
  -o --out=<file>                       Write output to file
  --order-trees                         Order trees for diff-root
  --platform=<platform>                 Use different platform for download
  --project=<project>                   Use different project for download
  -q --quiet                            Show less information, only warnings and errors
  --ref-file=<file>                     Reference file to compare with
  --run-all-tests                       Ignores art-include headers, runs all tests
  --seq=<N>                             Use N as postfix on destination nightly-tag (for retries) [default: 0]
  --test-type=<TT>                      Type of test (e.g. all, batch or single) [default: all]
  --timeout=<S>                         Timeout of test in seconds [default: 0]
  --tmp=<dir>                           Temporary directory for downloaded files and caching of EXT0
  --txt-file=<file>...                  Compare the following txt files
  --type=<T>                            Type of job (e.g. grid, build)
  --unpack                              Unpack downloaded tar files
  --user=<user>                         User to use for RUCIO
  -v --verbose                          Show more information, debug level
  --version                             Show version
  --work-dir=<dir>                      Working directory [default: .]

Sub-commands:
  run               Run jobs from a package in a local build (needs release and grid setup)
  grid              Run jobs from a package on the grid (needs release and grid setup)
  submit            Submit nightly jobs to the grid and informs big panda (NOT for users)
  copy              Copy outputs and logs from RUCIO
  validate          Check headers in tests
  included          Show list of files which will be included for art submit/art grid
  compare           Compare the output of a job
  list              List the jobs of a package
  log               Show the log of a job
  output            Get the output of a job
  config            Show configuration
  createpoolfile    Creates an 'empty' poolfile catalog
  download          Downloads the output of a reference job for comparison
  input-collect     Scan latest test directories for declarations of art-input
  input-merge       Merges all input files and compares to reference file

Arguments:
  indexed_package   Package of the test or indexed package (e.g. MooPerformance.4)
  package           Package of the test (e.g. Tier0ChainTests)
  packages          List of packages (e.g. Tier0ChainTests)
  path              Directory or File to compare
  ref_path          Directory or File to compare to
  release           A 'nightly_release/project/platform' combination, no spaces; platform default = x86_64-slc6-gcc62-opt
  script_directory  Directory containing the package(s) with tests
  sequence_tag      Sequence tag (e.g. 0 or PIPELINE_ID)
  test_name         Name of the test inside the package (e.g. test_q322.sh)
  test_names        Names of the test inside the package (e.g. test_q322.sh test_q222.sh)

Environment:
  AtlasBuildBranch          Name of the nightly release (e.g. 21.0)
  AtlasProject              Name of the project (e.g. Athena)
  <AtlasProject>_PLATFORM   Platform (e.g. x86_64-slc6-gcc62-opt)
  AtlasBuildStamp           Nightly tag (e.g. 2017-02-26T2119)

Tests are called with:
  arguments: PACKAGE TEST_NAME SCRIPT_DIRECTORY TYPE [IN_FILE]
  environment: ArtScriptDirectory, ArtPackage, ArtJobType, ArtJobName, [ArtInFile]
"""
from __future__ import print_function

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"
__version__ = '1.0.23'

import logging
import os
import sys

# add ../python to library search path
sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python'))
import ART  # noqa: E402

from ART.docopt_dispatch import dispatch  # noqa: E402
from ART import ArtBase, ArtGrid, ArtBuild  # noqa: E402
from ART.art_misc import build_script_directory, get_atlas_env, set_log  # noqa: E402

MODULE = "art"

ARTPROD = 'artprod'

#
# First list the double commands
#


@dispatch.on('compare', 'ref')
def compare_ref(path, ref_path, **kwargs):
    """Compare the output of a job."""
    set_log(kwargs)
    files = kwargs['file']
    txt_files = kwargs['txt_file']
    diff_pool = kwargs['diff_pool']
    diff_root = kwargs['diff_root']
    if not txt_files and not diff_pool and not diff_root:
        diff_pool = True
        diff_root = True
    entries = kwargs['entries']
    mode = kwargs['mode']
    order_trees = kwargs['order_trees']
    out = kwargs['out']
    exit(ArtBase().compare_ref(path, ref_path, files, txt_files, diff_pool, diff_root, entries, mode, order_trees, out))


@dispatch.on('compare', 'grid')
def compare_grid(package, test_name, **kwargs):
    """Compare the output of a job."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    max_refs = int(kwargs['max_refs'])
    user = _get_user(kwargs)
    files = kwargs['file']
    txt_files = kwargs['txt_file']
    diff_pool = kwargs['diff_pool']
    diff_root = kwargs['diff_root']
    if not txt_files and not diff_pool and not diff_root:
        diff_pool = True
        diff_root = True
    entries = kwargs['entries']
    mode = kwargs['mode']
    order_trees = kwargs['order_trees']
    out = kwargs['out']
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag).compare(package, test_name, max_refs, user, files, txt_files, diff_pool, diff_root, entries=entries, mode=mode, order_trees=order_trees, shell=True, out=out))


@dispatch.on('list', 'grid')
def list(package, **kwargs):
    """List the jobs of a package."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    job_type = 'grid'
    index_type = kwargs['test_type']
    json_format = kwargs['json']
    user = _get_user(kwargs)
    out = kwargs['out']
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag).list(package, job_type, index_type, json_format, user, out))


@dispatch.on('log', 'grid')
def log(package, test_name, **kwargs):
    """Show the log of a job."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    user = _get_user(kwargs)
    out = kwargs['out']
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag).log(package, test_name, user, out))


@dispatch.on('output', 'grid')
def output(package, test_name, **kwargs):
    """Get the output of a job."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    user = _get_user(kwargs)
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag).output(package, test_name, user))


@dispatch.on('submit')
def submit(sequence_tag, **kwargs):
    """Submit nightly jobs to the grid, NOT for users."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    job_type = 'grid' if kwargs['type'] is None else kwargs['type']
    user = _get_user(None)
    inform_panda = user == ARTPROD
    packages = kwargs['packages']
    config = kwargs['config']
    no_action = kwargs['no_action']
    max_jobs = int(kwargs['max_jobs'])
    wait_and_copy = True
    run_all_tests = kwargs['run_all_tests']

    # only for unittest, make sure to use .get()
    script_directory = kwargs.get('script_directory', None)
    inform_panda = kwargs.get('inform_panda', inform_panda)
    wait = kwargs.get('wait', ArtGrid.INITIAL_RESULT_WAIT_INTERVAL)

    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, script_directory=script_directory, max_jobs=max_jobs, run_all_tests=run_all_tests).task_list(job_type, sequence_tag, user, inform_panda, packages, no_action, wait_and_copy, config, wait=wait))


@dispatch.on('grid')
def grid(script_directory, sequence_tag, **kwargs):
    """Run jobs from a package on the grid, needs release and grid setup."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    job_type = 'grid' if kwargs['type'] is None else kwargs['type']
    inform_panda = False
    packages = []
    tests = []  # kwargs['test_names']
    config = None
    no_action = kwargs['no_action']
    max_jobs = int(kwargs['max_jobs'])
    wait_and_copy = False
    run_all_tests = kwargs['run_all_tests']
    user = _get_user(None)

    # only for unittest, make sure to use .get()
    wait = kwargs.get('wait', ArtGrid.INITIAL_RESULT_WAIT_INTERVAL)

    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, script_directory=script_directory, max_jobs=max_jobs, run_all_tests=run_all_tests).task_list(job_type, sequence_tag, user, inform_panda, packages, no_action, wait_and_copy, config, wait=wait, tests=tests))


@dispatch.on('run')
def run(script_directory, sequence_tag, **kwargs):
    """Run jobs from a package in a local build, needs release and grid setup."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    job_type = 'build' if kwargs['type'] is None else kwargs['type']
    tests = kwargs['test_names']
    run_all_tests = kwargs['run_all_tests']
    timeout = int(kwargs['timeout'])
    copy = kwargs['copy']
    exit(ArtBuild(art_directory, nightly_release, project, platform, nightly_tag, script_directory=script_directory, max_jobs=int(kwargs['max_jobs']), ci=kwargs['ci'], run_all_tests=run_all_tests).task_list(job_type, sequence_tag, timeout=timeout, copy=copy, tests=tests))


@dispatch.on('copy')
def copy(indexed_package, **kwargs):
    """Copy outputs to eos area."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    # NOTE: default depends on USER, not set it here but in ArtGrid.copy
    dst = kwargs['dst']
    user = _get_user(kwargs)
    default_dst = ArtGrid.EOS_OUTPUT_DIR if user == ARTPROD else '.'
    dst = default_dst if dst is None else dst
    unpack = kwargs['unpack']
    tmp = kwargs['tmp']
    seq = int(kwargs['seq'])
    keep_tmp = kwargs['keep_tmp']
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag).copy(indexed_package, dst, user, unpack=unpack, tmp=tmp, seq=seq, keep_tmp=keep_tmp))


@dispatch.on('validate')
def validate(**kwargs):
    """Check headers in tests."""
    set_log(kwargs)
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    script_directory = build_script_directory(kwargs['script_directory'], nightly_release, project, platform, nightly_tag)
    exit(ArtBase().validate(script_directory))


@dispatch.on('included')
def included(**kwargs):
    """Show list of files which will be included for art submit/art grid."""
    set_log(kwargs)
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    job_type = kwargs['type']   # None will list all types
    index_type = kwargs['test_type']
    out = kwargs['out']
    script_directory = build_script_directory(kwargs['script_directory'], nightly_release, project, platform, nightly_tag)
    result = ArtBase().included(script_directory, job_type, index_type, nightly_release, project, platform)
    file = sys.stdout if out is None else open(out, "w")
    for test_name, include in sorted(result.iteritems()):
        print(test_name, include, file=file)
    exit(0)


@dispatch.on('input-collect')
def input_collect(**kwargs):
    """Scan latest test directories for declarations of art-input."""
    set_log(kwargs)
    job_type = 'grid' if kwargs['type'] is None else kwargs['type']
    run_all_tests = kwargs['run_all_tests']
    copy = kwargs['copy']
    ART_INPUT_REFERENCE = '/eos/atlas/atlascerngroupdisk/data-art/grid-output/art-input-reference'
    copy = ART_INPUT_REFERENCE if copy is None else copy
    release = kwargs['release']
    exit(ArtBase().input_collect(job_type, run_all_tests, copy, release))


@dispatch.on('input-merge')
def input_merge(**kwargs):
    """Merge all input files and compares to reference file."""
    set_log(kwargs)
    work_dir = kwargs['work_dir']
    ref_file = kwargs['ref_file']
    exit(ArtBase().input_merge(work_dir=work_dir, ref_file=ref_file))


@dispatch.on('config')
def config(package, **kwargs):
    """Show configuration."""
    set_log(kwargs)
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    config = kwargs['config']
    exit(ArtBase().config(package, nightly_release, project, platform, config))


@dispatch.on('createpoolfile')
def createpoolfile(package, **kwargs):  # pragma: no cover
    """Show configuration."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag).createpoolfile())


@dispatch.on('download')
def download(package, test_name, **kwargs):
    """Download the output of a reference job."""
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    max_refs = int(kwargs['max_refs'])
    user = _get_user(kwargs)
    dst = kwargs['dst']
    ref_nightly_release = kwargs['nightly_release']
    ref_project = kwargs['project']
    ref_platform = kwargs['platform']

    # only for unittest, make sure to use .get()
    max_tries = kwargs.get('max_tries', 3)

    ref = ArtGrid(art_directory, nightly_release, project, platform, nightly_tag).download(package, test_name, max_refs, user, ref_dir=dst, shell=True, max_tries=max_tries, nightly_release=ref_nightly_release, project=ref_project, platform=ref_platform)
    exit(1 if ref is None else 0)


def _get_art_directory():
    """Retrieve the directory where art.py is installed."""
    artpath = __file__

    # Turn pyc files into py files if we can
    if artpath.endswith('.pyc') and os.path.exists(artpath[:-1]):  # pragma: no cover
        artpath = artpath[:-1]

    return os.path.dirname(os.path.realpath(artpath))


def _get_user(kwargs):
    """
    Retrieve user.

    By default ARTPROD user is used.
    For most commands the user can be overridden by --user=<USER>.
    For "art grid" the logged in USER is used, as this is a user command.
    For "art submit" the logged in USER (ARTPROD) is used.

    When run in the grid, the user may be anything, which is why the default user is ARTPROD
    """
    user = None

    # kwargs is None, no option override, e.g. art grid and art submit
    user = os.getenv('USER', ARTPROD) if kwargs is None else user

    # --user, override option given
    user = kwargs.get('user', None) if user is None else user

    # default, ARTPROD
    user = ARTPROD if user is None else user

    return user


if __name__ == '__main__':  # pragma: no cover
    if sys.version_info < (2, 7, 0):
        sys.stderr.write("You need python 2.7 or later to run this script\n")
        exit(1)

    logging.basicConfig()
    logger = logging.getLogger(MODULE)
    logger.setLevel(logging.INFO)
    logger.info("ART      %s", os.path.dirname(os.path.realpath(__file__)))
    logger.info("ART_PATH %s", _get_art_directory())
    logger.info("ART_LIB  %s", os.path.dirname(os.path.dirname(ART.__file__)))
    dispatch(__doc__, version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)
