#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""ART Trigger Script."""

import argparse
import datetime
import getpass
import os
import sys

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"
__version__ = '1.0.23'


def main(args):
    """ART Trigger."""
    parser = argparse.ArgumentParser(description='ART Submit Trigger')
    parser.add_argument('--submit', action="store_true", default=False, help="Submits job to Grid")
    parser.add_argument('--run-all-tests', action="store_true", default=False, help="Runs all tests, ignoring art-include headers")
    parser.add_argument('-n', '--no-action', action="store_true", default=False, help="Submit to gitlab with --no-action")
    parser.add_argument('--no-wait', action="store_true", default=False, help="Do not wait for distribution of nightly")
    parser.add_argument('--branch', action="store", default='master', help="Use branch/tag for submitting [default: master]")
    parser.add_argument('--grid', action="store_true", default=False, help="Only submit jobs to the grid")
    parser.add_argument('--build', action="store_true", default=False, help="Only submit jobs to the build machines")
    parser.add_argument('--timeout', action="store", default='0', type=int, help="Specify timeout for the jobs")
    parser.add_argument('--version', action='version', version='%(prog)s ' + __version__)
    parser.add_argument('NIGHTLY_RELEASE')
    parser.add_argument('PROJECT')
    parser.add_argument('PLATFORM')
    parser.add_argument('NIGHTLY_TAG')
    parser.add_argument('PACKAGE', nargs='*')

    results = parser.parse_args(args)

    print "INFO: art-trigger", __version__, "executed by", getpass.getuser(), "on", datetime.datetime.today()

    token = os.getenv('ART_TOKEN', None)
    if token is None:  # pragma: no cover
        print "Set ART_TOKEN using"
        print "export ART_TOKEN='xxxxxx'"
        sys.exit(-1)

    url = 'https://gitlab.cern.ch/api/v4/projects/art%2Fart-submit/trigger/pipeline'

    trigger = ' '.join((
        'curl',
        '-X POST',
        '-F token=' + token,
        '-F ref=' + results.branch,
        '-F variables[NIGHTLY_RELEASE]=' + results.NIGHTLY_RELEASE,
        '-F variables[PROJECT]=' + results.PROJECT,
        '-F variables[PLATFORM]=' + results.PLATFORM,
        '-F variables[NIGHTLY_TAG]=' + results.NIGHTLY_TAG,
        '-F variables[PACKAGE]=' + results.PACKAGE[0] if results.PACKAGE else '',   # TBR when we release 1.0
        '-F variables[PACKAGES]="' + ' '.join(results.PACKAGE) + '"' if results.PACKAGE else '',
        '-F variables[NO_ACTION]=true' if results.no_action else '',
        '-F variables[NO_WAIT]=true' if results.no_wait else '',
        '-F variables[GRID]=true' if results.grid or (not results.grid and not results.build) else '',
        '-F variables[BUILD]=true' if results.build or (not results.grid and not results.build) else '',
        '-F variables[RUN_ALL_TESTS]=true' if results.run_all_tests else '',
        '-F variables[TIMEOUT]=' + str(results.timeout) if results.timeout > 0 else '',
        url
    ))

    if results.submit:  # pragma: no cover
        os.system(trigger)
    else:
        print "Not executing:", trigger

    sys.exit(0)


if __name__ == '__main__':  # pragma: no cover
    main(sys.argv[1:])
