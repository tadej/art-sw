#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""
ART-internal - ATLAS Release Tester (internal command).

Usage:
  art-internal.py grid batch  [-v -q -n --run-all-tests] <script_directory> <sequence_tag> <package> <outfile> <inform_panda> <job_type> <job_index>
  art-internal.py grid single [-v -q --in=<in_file> -n --run-all-tests] <script_directory> <sequence_tag> <package> <outfile> <inform_panda> <job_name>

Options:
  -h --help         Show this screen.
  --in=<in_file>    Normally percentage IN
  -n --no-action    No real submit will be done
  -q --quiet        Show less information, only warnings and errors
  --run-all-tests   Runs all tests, ignoring art-include
  -v --verbose      Show more information, debug level
  --version         Show version.

Arguments:
  inform_panda      Inform Big Panda about job
  job_index         Index of the test inside the package
  job_name          Index of the test (batch), or its name (single)
  job_type          Type of job (e.g. grid, ci, build)
  outfile           Tar filename used for the output of the job
  package           Package of the test (e.g. Tier0ChainTests)
  script_directory  Directory containing the package(s) with tests
  sequence_tag      Sequence tag (e.g. 0 or PIPELINE_ID)
  submit_directory  Temporary directory with all files for submission

Environment:
  AtlasBuildBranch          Name of the nightly release (e.g. 21.0)
  AtlasProject              Name of the project (e.g. Athena)
  <AtlasProject>_PLATFORM   Platform (e.g. x86_64-slc6-gcc62-opt)
  AtlasBuildStamp           Nightly tag (e.g. 2017-02-26T2119)
"""
from __future__ import print_function

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import logging
import os
import sys

# add ../python to library search path
sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python'))
import ART  # noqa: E402

from ART.docopt_dispatch import dispatch  # noqa: E402
from ART import ArtGrid  # noqa: E402
from ART.art_misc import get_atlas_env, set_log  # noqa: E402

MODULE = "art.internal"


@dispatch.on('grid', 'batch')
def grid_batch(script_directory, sequence_tag, package, outfile, inform_panda, job_type, job_index, **kwargs):
    """Run a batch job, given a particular index.

    Tests are called with the following parameters:
    SCRIPT_DIRECTORY, PACKAGE, TYPE, TEST_NAME, STAGE
    """
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    run_all_tests = kwargs['run_all_tests']
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, script_directory=script_directory, run_all_tests=run_all_tests).batch(sequence_tag, package, outfile, inform_panda, job_type, job_index))


@dispatch.on('grid', 'single')
def grid_single(script_directory, sequence_tag, package, outfile, inform_panda, job_name, **kwargs):
    """Run a single job, given a particular name.

    Tests are called with the following parameters:
    SCRIPT_DIRECTORY, PACKAGE, TYPE, TEST_NAME, STAGE, IN_FILE
    """
    set_log(kwargs)
    art_directory = _get_art_directory()
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    in_file = kwargs['in']
    run_all_tests = kwargs['run_all_tests']
    exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, script_directory=script_directory, run_all_tests=run_all_tests).single(sequence_tag, package, outfile, inform_panda, job_name, in_file))


def _get_art_directory():
    """Retrieve the directory where art.py is installed."""
    artpath = __file__

    # Turn pyc files into py files if we can
    if artpath.endswith('.pyc') and os.path.exists(artpath[:-1]):
        artpath = artpath[:-1]

    return os.path.dirname(os.path.realpath(artpath))


if __name__ == '__main__':  # pragma: no cover
    if sys.version_info < (2, 7, 0):
        sys.stderr.write("You need python 2.7 or later to run this script\n")
        exit(1)

    # NOTE: import should be here, to keep the order of the decorators (module first, art last and unused)
    from art import __version__
    logging.basicConfig()
    log = logging.getLogger(MODULE)
    log.setLevel(logging.INFO)
    log.info("ART      %s", os.path.dirname(os.path.realpath(__file__)))
    log.info("ART_PATH %s", _get_art_directory())
    log.info("ART_LIB  %s", os.path.dirname(os.path.dirname(ART.__file__)))
    dispatch(__doc__, version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)
