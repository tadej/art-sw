#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""TBD."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import logging
import os
import unittest

from ART.art_grid import ArtGrid
from ART.art_testcase import ArtTestCase
from ART.art_misc import set_log_level


class TestArtGrid(ArtTestCase):
    """Unittests for Art Grid."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArtGrid, self).__init__(*args, **kwargs)
        self.cwd = os.getcwd()

    def setUp(self):
        """Setup for Grid Tests."""
        set_log_level(logging.WARN)
        os.chdir(self.cwd)
        self.nightly_release = 'master'
        self.project = 'Athena'
        self.platform = 'x86_64-slc6-gcc8-opt'
        self.nightly_tag = '2019-01-08T2349'
        self.grid = ArtGrid(self.art_directory, self.nightly_release, self.project, self.platform, self.nightly_tag, script_directory=None, max_jobs=0, run_all_tests=False)

    def test_batch(self):
        """Testing batch job."""
        seq = os.getenv('CI_PIPELINE_ID', None)
        self.assertIsNotNone(seq)
        grid = ArtGrid(self.art_directory, 'master', 'Athena', 'x86_64-slc6-gcc8-opt', '2019-01-08T2349', script_directory='test/data_art_grid', max_jobs=0, run_all_tests=True)
        grid.batch(seq, "Package-001", out="test_batch.tar", inform_panda=False, job_type="grid", job_index=1)
        # check for output tar and json
        self.assertTrue(os.path.isfile('test_batch.tar'))
        self.assertTrue(os.path.isfile('jobReport.json'))

    def test_single(self):
        """Testing single job."""
        seq = os.getenv('CI_PIPELINE_ID', None)
        self.assertIsNotNone(seq)
        grid = ArtGrid(self.art_directory, 'master', 'Athena', 'x86_64-slc6-gcc8-opt', '2019-01-08T2349', script_directory='test/data_art_grid', max_jobs=0, run_all_tests=True)
        grid.single(seq, "Package-001", out="test_single.tar", inform_panda=False, job_name="test_grid2.sh", in_file="user.artprod.user.iconnell.410470.DAOD_TOPQ1.e6337_a875_r10201_p3554.ART.v2")
        # check for output tar and json
        self.assertTrue(os.path.isfile('test_batch.tar'))
        self.assertTrue(os.path.isfile('jobReport.json'))

    def test_get_previous_nightly_tags(self):
        """Test for get_previous_nightly_tags."""
        self.assertEqual(self.grid._ArtGrid__get_previous_nightly_tags(2, self.nightly_release, self.project, self.platform, self.nightly_tag, directory=os.path.join(self.test_directory, 'data_art_grid/repo/sw')), ['2019-01-06T2351', '2019-01-05T2350'])
        self.assertEqual(self.grid._ArtGrid__get_previous_nightly_tags(6, self.nightly_release, self.project, self.platform, self.nightly_tag, directory=os.path.join(self.test_directory, 'data_art_grid/repo/sw')), ['2019-01-06T2351', '2019-01-05T2350', '2019-01-03T2350'])

    def test_inform_panda(self):
        """Test with mockup inform panda."""
        panda_id = "107"
        job_name = "DummyJobName"
        package = "DummyPackage"
        self.assertTrue(self.grid.inform_panda(panda_id, job_name, package, os.path.join(self.test_directory, 'data_art_grid'), url="https://atlas-project-art.web.cern.ch/atlas-project-art/inform_panda/?json"))

    def test_inform_panda_incorrect_url(self):
        """Test with non-existing inform panda."""
        panda_id = "108"
        job_name = "DummyJobName"
        package = "DummyPackage"
        self.assertFalse(self.grid.inform_panda(panda_id, job_name, package, os.path.join(self.test_directory, 'data_art_grid'), url="https://atlas-project-art.web.cern.ch/atlas-project-art/unknown_url/?json"))


if __name__ == '__main__':
    unittest.main()
