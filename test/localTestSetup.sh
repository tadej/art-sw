# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
# regular command setup
SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
export PYTHONPATH=${SCRIPTPATH}/../test:${PYTHONPATH}
export PYTHONPATH=${SCRIPTPATH}/../scripts:${PYTHONPATH}
