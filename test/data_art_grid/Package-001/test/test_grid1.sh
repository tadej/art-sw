#!/bin/sh

# art-description: Batch job
# art-type: grid
# art-include: master/Athena
# art-output: *.txt

# Create empty PoolFileCatalog
art.py createpoolfile

echo "hello" >> msg.txt
echo  "art-result: $? print"

art.py compare ref . /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/ART
echo  "art-result: $? diff"
