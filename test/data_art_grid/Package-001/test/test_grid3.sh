#!/bin/sh

# art-description: single job on ncore
# art-type: grid
# art-input: user.artprod.user.iconnell.410470.DAOD_TOPQ1.e6337_a875_r10201_p3554.ART.v2
# art-input-nfiles: 1
# art-cores: 3
# art-include: master/Athena
# art-output: *.txt

# Create empty PoolFileCatalog
art.py createpoolfile

echo "ArtProcess: $ArtProcess"

case $ArtProcess in
    
    "start")
	echo "Starting"

	;;

    "end")
	echo "Ending"
	echo  "art-result: $? final"

	;;

    *)
	echo "Processing"
	echo "Test $ArtProcess"

	mkdir "art_core_${ArtProcess}"
	cd "art_core_${ArtProcess}"

	echo "hello" >> msg.txt
	echo  "art-result: $? print"

	art.py compare ref . /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/ART
	echo  "art-result: $? diff"

	;;
esac

