#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""TBD."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import logging
import os
import unittest

from ART.art_configuration import ArtConfiguration
from ART.art_grid import ArtGrid
from ART.art_rucio import ArtRucio
from ART.art_testcase import ArtTestCase
from ART.art_misc import find, set_log_level


class TestArtRucio(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArtRucio, self).__init__(*args, **kwargs)
        self.cwd = os.getcwd()

    def setUp(self):
        """TBD."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)
        self.user = 'artprod'
        self.nighly_release = '21.0'
        self.nightly_tag = '2019-06-03T2212'
        self.project = 'Athena'
        self.platform = 'x86_64-slc6-gcc62-opt'
        self.rucio = ArtRucio(self.art_directory, self.nighly_release, 'Athena', 'x86_64-slc6-gcc62-opt')
        self.prefix = '.'.join(('user', self.user, 'atlas', self.nighly_release, self.project, self.platform, self.nightly_tag))

    def test_get_scope(self):
        """TBD."""
        self.assertEqual(self.rucio._ArtRucio__get_scope(self.user), 'user.' + self.user)

    def test_parse_outfile(self):
        """TBD."""
        self.wrong_nightly_tag = '.'.join(('user', self.user, 'atlas', self.nighly_release, self.project, self.platform, '2018-01-21T2301'))
        self.assertEqual(self.rucio._ArtRucio__parse_outfile(self.wrong_nightly_tag + '.896696.MuonRecRTT.6', self.nightly_tag), None)
        self.assertEqual(self.rucio._ArtRucio__parse_outfile(self.prefix + '.283573.TrigAnalysisTest', self.nightly_tag), ('283573', -1, -1))
        self.assertEqual(self.rucio._ArtRucio__parse_outfile(self.prefix + '.896696.MuonRecRTT.6', self.nightly_tag), ('896696', 6, -1))
        self.assertEqual(self.rucio._ArtRucio__parse_outfile(self.prefix + '.284777.MuonRecRTT.6.log.13062437.000001.log.tgz', self.nightly_tag), ('284777', 6, 1))
        self.assertEqual(self.rucio._ArtRucio__parse_outfile(self.prefix + '.896696.MuonRecRTT.log.13062437.000007', self.nightly_tag), ('896696', -1, 7))

    def test_get_sequence_tag(self):
        """TBD."""
        self.assertEqual(self.rucio.get_sequence_tag(self.prefix + '.284777.MuonRecRTT.6.log.13062437.000001.log.tgz', self.nightly_tag), '284777')

    def test_get_single_index(self):
        """TBD."""
        self.assertEqual(self.rucio.get_single_index(self.prefix + '.284777.MuonRecRTT.6.log.13062437.000001.log.tgz', self.nightly_tag), 6)

    def test_get_grid_index(self):
        """TBD."""
        self.assertEqual(self.rucio.get_grid_index(self.prefix + '.284777.MuonRecRTT.6.log.13062437.000001.log.tgz', self.nightly_tag), 1)

    def test_get_outfile_name(self):
        """TBD."""
        seq = '007'
        package = 'Tier0ChainTests'
        self.assertEqual(self.rucio.get_outfile_name(self.user, package, seq, self.nightly_tag, 'test_name.sh'), '.'.join((self.prefix, seq, package, 'test_name.sh')))
        self.assertEqual(self.rucio.get_outfile_name(self.user, package, seq, self.nightly_tag, test_name='test_name.sh'), '.'.join((self.prefix, seq, package, 'test_name.sh')))
        self.assertEqual(self.rucio.get_outfile_name(self.user, package, seq, self.nightly_tag), '.'.join((self.prefix, seq, package)))

    def test_list_dids(self):
        """TBD."""
        self.assertEqual(self.rucio._ArtRucio__list_dids("user.artprod.atlas.21.0.Athena.x86_64-slc6-gcc62-opt.2019-07-24T2147.*.Tier0ChainTests.log", True), ["user.artprod.atlas.21.0.Athena.x86_64-slc6-gcc62-opt.2019-07-24T2147.997898.Tier0ChainTests.log"])

    # NOTE: if you change release, ..., you need to run this test once to get the number
    def test_get_outfiles(self):
        """TBD."""
        self.assertEqual(self.rucio.get_outfiles(self.user, 'Tier0ChainTests', self.nightly_tag), [self.prefix + '.896696.Tier0ChainTests'])

    def test_table(self):
        """TBD."""
        # see full table if different
        self.maxDiff = None
        table = self.rucio.get_table(self.user, 'Tier0ChainTests', self.nightly_tag)
        self.assertNotEqual(table, None)
        # self.assertEqual(len(table), 29)

    def test_result_copy(self):
        """Test last part of the task_list, i.e. the download."""
        grid = ArtGrid(self.art_directory, '21.0', 'Athena', 'x86_64-slc6-gcc62-opt', 'latest', script_directory=None, max_jobs=0, run_all_tests=False)

        results = {
            -100: ("ISF_Validation", "n/a", "n/a", 0, None),
            -200: ("SimCoreTests", "n/a", "n/a", 0, None)
        }
        sequence_tag = "00000"
        user = "artprod"
        configuration = ArtConfiguration('test/data_art_rucio/art-configuration.yml')
        wait = 4  # seconds
        copies = grid.task_results(results, sequence_tag, user, configuration, wait)
        self.assertEqual(grid.task_copies(copies), 0)
        find('/tmp/output')


if __name__ == '__main__':
    unittest.main()
