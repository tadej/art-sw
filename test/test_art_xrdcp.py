#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""TBD."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import logging
import os
import shutil
import unittest

from ART.art_misc import set_log_level, xrdcp
from ART.art_testcase import ArtTestCase


class TestArtXrdcp(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArtXrdcp, self).__init__(*args, **kwargs)
        self.cwd = os.getcwd()

    def setUp(self):
        """TBD."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)

        # for xrdcp tests
        self.copy_dir = 'tmp-unitbuild-copy'
        self.src_dir = os.path.join(self.test_directory, 'data_art_xrdcp')
        self.src_file = os.path.join(self.src_dir, 'test.data')
        self.dst_dir = os.path.join(self.copy_dir, 'dst_dir')
        self.dst_file = os.path.join(self.dst_dir, 'test.data')
        self.rmt_dir = '/eos/atlas/atlascerngroupdisk/data-art/build-output/test/unittests/xrdcp/dst_dir'
        self.rmt_file = os.path.join(self.rmt_dir, 'test.data')

    def test_xrdcp_file_dir_local(self):
        """TBD."""
        if os.path.isdir(self.copy_dir):
            shutil.rmtree(self.copy_dir)
        self.assertEqual(xrdcp(self.src_file, self.dst_dir), 0)
        self.assertEqual(self.read_text(self.dst_file), self.read_text(self.src_file))

    def test_xrdcp_dir_dir_local(self):
        """TBD."""
        if os.path.isdir(self.copy_dir):
            shutil.rmtree(self.copy_dir)
        self.assertEqual(xrdcp(self.src_dir, self.dst_dir), 0)
        self.assertEqual(self.read_text(self.dst_file), self.read_text(self.src_file))

    def xtest_xrdcp_file_dir_remote(self):
        """TBD."""
        self.assertEqual(xrdcp(self.src_file, self.rmt_dir), 0)
        self.assertEqual(self.read_text(self.dst_file), self.read_text(self.rmt_file))

    def xtest_xrdcp_dir_dir_remote(self):
        """TBD."""
        self.assertEqual(xrdcp(self.src_dir, self.rmt_dir), 0)
        self.assertEqual(self.read_text(self.dst_file), self.read_text(self.rmt_file))


if __name__ == '__main__':
    unittest.main()
