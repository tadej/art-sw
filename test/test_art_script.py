#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""TBD."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import glob
import logging
import os
import shutil
import sys
import unittest

from ART.art_testcase import ArtTestCase
from ART.art_misc import cat, set_log_level
from ART.docopt_dispatch import dispatch
from art import __doc__ as __art_doc__
from art import ARTPROD, download, submit, _get_user


class TestArt(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArt, self).__init__(*args, **kwargs)

        # correct location where we would have found art.py
        sys.argv[0] = os.path.join(self.art_directory, 'art.py')

        # to find art.py script and its doc
        sys.path.append(self.art_directory)

        self.cwd = os.getcwd()

    def setUp(self):
        """Setting up Build Tests."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)

    def test_art_compare_ref(self):
        """Test art compare ref command."""
        curdir = '.'
        refdir = 'test/data_art_grid/Package-001/test'
        file = 'test_grid2.sh'
        shutil.copyfile(os.path.join(refdir, file), os.path.join(curdir, file))
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['compare', 'ref', '-v', '--txt-file=' + file, '--out=test_art_compare_ref.txt', curdir, refdir])
        self.assertEqual(cm.exception.code, 0)

    def test_art_compare_grid(self):
        """Test art compare grid command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['compare', 'grid', '-v', '--max-refs=16', '--out=test_art_compare_grid.txt', 'Tier0ChainTests', 'test_q431'])
        cat('test_art_compare_grid.txt')
        # always finds a difference
        self.assertEqual(cm.exception.code, 255)

    def test_art_compare_ref_non_existing_dir(self):
        """Test art compare ref non-existing-dir command."""
        curdir = '.'
        refdir = 'test/non-existing-dir/Package-001/test'
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['compare', 'ref', '-v', '--diff-pool', '--out=test_art_compare_ref_non_existing_dir.txt', curdir, refdir])
        self.assertEqual(cm.exception.code, 255)

    def test_art_config(self):
        """Test art config command."""
        print "CWD", os.getcwd()
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['config', '--config', 'test/data_art_configuration/art-configuration.yml', 'Tier0ChainTests'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_copy(self):
        """Test art copy command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['copy', '--dst=/tmp', '1'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_download(self):
        """Test art download command."""
        print "CWD", os.getcwd()
        with self.assertRaises(SystemExit) as cm:
            download(package='Tier0ChainTests',
                     test_name='test_q431',
                     max_tries=1,
                     max_refs=16,
                     user=None,
                     dst=None,
                     nightly_release=None,
                     project=None,
                     platform=None
                     )
        self.assertEqual(cm.exception.code, 0)

    def test_get_user(self):
        """Test get_user."""
        self.assertEqual(_get_user(None), os.getenv('USER', ARTPROD))
        self.assertEqual(_get_user({'user': 'art'}), 'art')
        self.assertEqual(_get_user({}), ARTPROD)

    def test_art_grid_no_action(self):
        """Test art grid --no-action command."""
        print "CWD", os.getcwd()
        seq = os.getenv('CI_PIPELINE_ID', None)
        self.assertIsNotNone(seq)
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['grid', '--no-action', '--run-all-tests', 'test/data_art_grid', seq])
        self.assertEqual(cm.exception.code, 0)

    def test_art_included(self):
        """Test art included command."""
        print "CWD", os.getcwd()
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['included', '--out=test_art_included.txt'])
        self.assertEqual(cm.exception.code, 0)
        self.assertTrue(os.path.isfile('test_art_included.txt'))

    def test_art_input_collect(self):
        """Test art input-collect command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['input-collect', '--copy=.', '21.0/Athena'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_input_merge(self):
        """Test art input-merge command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['input-merge'])
        self.assertEqual(cm.exception.code, 0)

    def test_art_list_grid(self):
        """Test art list grid command."""
        file = 'test_art_list_grid.txt'
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['list', 'grid', '--out=' + file, 'Tier0ChainTests'])
        self.assertEqual(cm.exception.code, 0)
        self.assertTrue(os.path.isfile(file))

    def test_art_list_json(self):
        """Test art list --json command."""
        file = 'test_art_list_grid_json.txt'
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['list', 'grid', '--json', '--out=' + file, 'Tier0ChainTests'])
        self.assertEqual(cm.exception.code, 0)
        self.assertTrue(os.path.isfile(file))

    def test_art_log(self):
        """Test art log command."""
        file = 'test_art_log.txt'
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['log', 'grid', '--out=' + file, 'Tier0ChainTests', 'test_q431'])
        if cm.exception.code == 0:
            pass
        #     self.assertTrue(os.path.isfile(file))

    def test_art_output(self):
        """Test art output command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['output', 'grid', 'Tier0ChainTests', 'test_q431'])
        if cm.exception.code == 0:
            dir = '.'.join(('user', 'artprod', 'atlas', '21.0', 'Athena', 'x86_64-slc6-gcc62-opt', '*', '*', 'Tier0ChainTests'))
            dir = os.path.join('/tmp', dir, 'test_q431')
            dir = glob.glob(dir)[0]
            self.assertTrue(os.path.isdir(dir))
            shutil.rmtree(dir, ignore_errors=True)

    def test_art_run(self):
        """Test art run command."""
        seq = os.getenv('CI_PIPELINE_ID', None)
        self.assertIsNotNone(seq)
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['run', 'test/results', seq])
        self.assertEqual(cm.exception.code, 0)

    def test_art_submit_no_action(self):
        """Test art submit --no-action command."""
        seq = os.getenv('CI_PIPELINE_ID', None)
        self.assertIsNotNone(seq)
        with self.assertRaises(SystemExit) as cm:
            submit(seq,
                   type=None,
                   packages=None,
                   config=None,
                   max_jobs=0,
                   no_action=True,
                   inform_panda=False,
                   run_all_tests=True,
                   script_directory='test/data_art_grid'
                   )
        self.assertEqual(cm.exception.code, 0)

    def test_art_validate(self):
        """Test art validate command."""
        with self.assertRaises(SystemExit) as cm:
            dispatch(__art_doc__, argv=['validate', 'test/data_art_header'])
        self.assertEqual(cm.exception.code, 1)


if __name__ == '__main__':
    unittest.main()
