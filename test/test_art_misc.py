#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""TBD."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import concurrent.futures
import logging
import os
import time
import unittest

from ART.art_misc import build_script_directory, count_files, GByte, get_atlas_env, get_release
from ART.art_misc import interrupt_command, ls, make_executable, memory, mkdir, rm, run_command
from ART.art_misc import run_command_parallel, search, set_log, set_log_level, split_release
from ART.art_misc import touch, uncomment_python, uncomment_sh, which
from ART.art_testcase import ArtTestCase


class TestArtMisc(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArtMisc, self).__init__(*args, **kwargs)
        self.cwd = os.getcwd()

    def setUp(self):
        """TBD."""
        set_log_level(logging.WARN)
        os.chdir(self.cwd)

    def test_build_script_directory(self):
        """TBD."""
        self.assertEqual(build_script_directory('/test', '21.0', 'Athena', 'x86_64-slc6-gcc62-opt', '2018-12-08T2252'), '/test')
        self.assertEqual(build_script_directory(None, '21.0', 'Athena', 'non-existing-platform-opt', '2018-12-08T2252'), None)

    def test_file_utils(self):
        """TBD."""
        dir = 'tmp-unit'
        file = os.path.join(dir, 'tmp.sh')
        # file_copy = os.path.join(dir, 'tmp-copy.sh')
        self.assertEqual(mkdir(dir), 0)
        # second time, dir exists
        self.assertEqual(mkdir(dir), 0)
        self.assertEqual(touch(file), None)
        # cannot mkdir on a file
        self.assertEqual(mkdir(file), 1)
        # self.assertEqual(cp(file, file_copy), 0)
        self.assertEqual(make_executable(file), None)
        self.assertEqual(ls(dir), 0)
        self.assertEqual(count_files(dir), 2)
        self.assertEqual(rm(dir), None)
        self.assertNotEqual(which('art.py'), '')
        self.assertNotEqual(which('/bin/sh'), '')
        self.assertEqual(which('xxx-non-existing'), None)

    def test_get_atlas_env(self):
        """TBD."""
        with self.assertRaises(SystemExit) as cm:
            get_atlas_env()
        self.assertEqual(cm.exception.code, 1)
        # self.assertEqual(os.environ['AtlasBuildBranch'], '21.0')
        # self.assertEqual(os.environ['AtlasProject'], 'Athena')
        # self.assertEqual((os.environ['Athena_PLATFORM'] == 'x86_64-slc6-gcc62-opt') or (os.environ['Athena_PLATFORM'] == 'x86_64-slc6-gcc8-opt'), True)
        # self.assertEqual(os.environ['AtlasBuildStamp'], 'latest')

    def test_get_release(self):
        """TBD."""
        script_directory = '/cvmfs/atlas-nightlies.cern.ch/repo/sw/21.0/2018-12-08T2252/Athena/21.0.91/InstallArea/x86_64-slc6-gcc62-opt/src'
        (nightly_release, project, platform, nightly_tag) = get_release(script_directory)
        self.assertEqual(nightly_release, '21.0')
        self.assertEqual(project, 'Athena')
        self.assertEqual(platform, 'x86_64-slc6-gcc62-opt')
        self.assertEqual(nightly_tag, '2018-12-08T2252')
        (nightly_release, project, platform, nightly_tag) = get_release("")
        self.assertEqual(nightly_release, None)
        self.assertEqual(project, None)
        self.assertEqual(platform, None)
        self.assertEqual(nightly_tag, None)

    def test_memory(self):
        """TBD."""
        self.assertGreater(memory(), 4 * GByte)

    def test_run_command(self):
        """TBD."""
        (exit_code, output, err, cmd, start_time, end_time, timed_out) = run_command("echo 'HelloWorld'", verbose=True)
        self.assertEqual(exit_code, 0)
        self.assertEqual(output, 'HelloWorld\n')
        self.assertEqual(err, '')
        self.assertEqual(cmd, "echo 'HelloWorld'")

    def test_run_command_pipe(self):
        """TBD."""
        exec_file = "cat " + os.path.join(self.art_install_directory, "README.md") + " | sort"
        (exit_code, output, err, cmd, start_time, end_time, timed_out) = run_command(exec_file, verbose=True)
        self.assertEqual(exit_code, 0)
        self.assertFalse(timed_out)
        self.assertEqual(err, '')
        self.assertEqual(cmd, exec_file)

    def test_run_command_error(self):
        """TBD."""
        exec_file = "cat " + os.path.join(self.art_install_directory, "non-existing-file") + " | cat another-non-existing-file"
        (exit_code, output, err, cmd, start_time, end_time, timed_out) = run_command(exec_file, verbose=True)
        self.assertEqual(exit_code, 1)
        self.assertFalse(timed_out)
        self.assertEqual(err, 'cat: ' + os.path.join(self.art_install_directory, "non-existing-file") + ': No such file or directory\ncat: another-non-existing-file: No such file or directory\n')
        self.assertEqual(cmd, exec_file)

    def test_run_command_timeout(self):
        """Check timeout on single command."""
        start = time.time()
        (exit_code, output, err, cmd, start_time, end_time, timed_out) = run_command("sleep 5", timeout=2, verbose=True)
        self.assertEqual(exit_code, 1)
        self.assertTrue(timed_out)
        self.assertEqual(err, '\nProcess timed out after 2 seconds.\n')
        self.assertEqual(output, '')
        self.assertEqual(cmd, 'sleep 5')
        end = time.time()
        duration = end - start
        self.assertLess(duration, 3.0)

    def test_run_command_interrupt(self):
        """Check interrupt on a single command."""
        start = time.time()
        timeout = 10
        limit = 15
        future_job_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)

        for sleep_time in [5, 30, 40, 50]:
            proc = {}
            future_task = executor.submit(run_command, "sleep " + str(sleep_time), verbose=True, proc=proc)
            future_job_set[future_task] = proc

        try:
            for future_task in concurrent.futures.as_completed(future_job_set.keys(), timeout=timeout):
                print("Done", future_task.result())
            # should NOT go here but timeout
        except concurrent.futures._base.TimeoutError:
            # timeout and interrupt job in time limit
            interrupted_set = concurrent.futures.wait(future_job_set.keys(), timeout=0).not_done
            print interrupted_set
            for future_task in interrupted_set:
                future_task.cancel()
                if 'value' in future_job_set[future_task]:
                    print "Interrupting", future_job_set[future_task]['value'].pid
                    interrupt_command(future_job_set[future_task]['value'])
            end = time.time()
            duration = end - start
            self.assertLess(duration, limit)

            # make sure job is interrupted in time limit
            for future_task in interrupted_set:
                try:
                    print("Interrupted", future_task.result())
                except concurrent.futures._base.CancelledError:
                    print("Cancelled", future_task)
            end = time.time()
            duration = end - start
            self.assertLess(duration, limit)

        # ends up here after timeout in time limit
        end = time.time()
        duration = end - start
        self.assertLess(duration, limit)

    def test_run_command_parallel(self):
        """TBD."""
        # Once Verbose, Once not
        exec_file = os.path.join(self.test_directory, 'data_art_misc/parallel_test.sh')
        verbose = True
        (exit_code, output, err, cmd, start_time, end_time, timed_out) = run_command_parallel(exec_file, 8, 4, verbose=verbose)
        self.assertEqual(exit_code, 0)
        if not verbose:
            self.assertEqual(output, '\nArtThreads: 8\nArtCores 4\nArtProcess start\nStarting\n\nArtThreads: 8\nArtCores 4\nArtProcess 0\nTest 0\n\nArtThreads: 8\nArtCores 4\nArtProcess 1\nTest 1\n\nArtThreads: 8\nArtCores 4\nArtProcess 2\nTest 2\n\nArtThreads: 8\nArtCores 4\nArtProcess 3\nTest 3\n\nArtThreads: 8\nArtCores 4\nArtProcess 4\nTest 4\n\nArtThreads: 8\nArtCores 4\nArtProcess 5\nTest 5\n\nArtThreads: 8\nArtCores 4\nArtProcess 6\nTest 6\n\nArtThreads: 8\nArtCores 4\nArtProcess 7\nTest 7\n\nArtThreads: 8\nArtCores 4\nArtProcess end\nEnding\n')
            self.assertEqual(err, '')
        self.assertEqual(cmd, exec_file)

    def test_set_log(self):
        """TBD."""
        kwargs = {}
        kwargs['verbose'] = True
        set_log(kwargs)

    def test_split_release(self):
        """Test split release."""
        (nightly_release, project, platform) = split_release('21.0/Athena/x86_64-slc6-gcc62-opt')
        self.assertEqual(nightly_release, '21.0')
        self.assertEqual(project, 'Athena')
        self.assertEqual(platform, 'x86_64-slc6-gcc62-opt')
        (nightly_release, project, platform) = split_release('21.0/Athena', platform='x86_64-slc6-gcc62-opt')
        self.assertEqual(nightly_release, '21.0')
        self.assertEqual(project, 'Athena')
        self.assertEqual(platform, 'x86_64-slc6-gcc62-opt')
        (nightly_release, project, platform) = split_release('21.0')
        self.assertEqual(nightly_release, '21.0')
        self.assertEqual(project, None)
        self.assertEqual(platform, None)
        (nightly_release, project, platform) = split_release("")
        self.assertEqual(nightly_release, None)
        self.assertEqual(project, None)
        self.assertEqual(platform, None)

    def test_uncomment(self):
        """TBD."""
        self.assertEqual(uncomment_sh('# art-include: 21.9/Athena'), '')
        self.assertEqual(uncomment_sh('line # comment'), 'line')
        self.assertEqual(uncomment_sh("False # perfmon with athenaHLT doesn't work at the moment"), 'False')
        self.assertEqual(uncomment_sh('"quoted # line" # comment'), 'quoted # line')
        self.assertEqual(uncomment_python('ex.args = \'-c "setMenu=\'LS2_v1\';doWriteBS=False;doWriteRDOTrigger=True;"\''), 'ex.args = \'-c "setMenu=\'LS2_v1\';doWriteBS=False;doWriteRDOTrigger=True;"\'')

    def test_search(self):
        """Test search."""
        status = [
            {
                "jeditaskid": 19573091,
                "taskname": "user.artprod.atlas.21.2.AnalysisBase.x86_64-centos7-gcc8-opt.2019-10-27T0347.1181424.SUSYTools/",
                "status": "failed",
            },
            {
                "jeditaskid": 19573090,
                "taskname": "user.artprod.atlas.21.2.AnalysisBase.x86_64-centos7-gcc8-opt.2019-10-27T0347.1181424.DirectIOART/",
                "status": "failed",
                "username": "artprod",
            },
        ]
        self.assertEqual(search(status, 'jeditaskid', 19573091), {'status': 'failed', 'taskname': 'user.artprod.atlas.21.2.AnalysisBase.x86_64-centos7-gcc8-opt.2019-10-27T0347.1181424.SUSYTools/', 'jeditaskid': 19573091})
        self.assertEqual(search(status, 'jeditaskid', 19573092), None)


if __name__ == '__main__':
    unittest.main()
