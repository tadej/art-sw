#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""TBD."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import logging
import os
import unittest

from ART.art_configuration import ArtConfiguration
from ART.art_testcase import ArtTestCase
from ART.art_misc import set_log_level


class TestArtConfiguration(ArtTestCase):
    """Unit Tests for ArtConfiguration."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArtConfiguration, self).__init__(*args, **kwargs)
        self.cwd = os.getcwd()

    def setUp(self):
        """TBD."""
        set_log_level(logging.WARN)
        os.chdir(self.cwd)

    def test_util(self):
        """TBD."""
        config = ArtConfiguration()
        self.assertEqual(config.release_key('21.0', 'Athena', 'x86_64-slc6-gcc62-opt'), '/21.0/Athena/x86_64-slc6-gcc62-opt')
        self.assertLess(config.release_key_compare('/20.9/Athena/x86_64-slc6-gcc62-opt', '/21.0/Athena/x86_64-slc6-gcc62-opt'), 0)
        self.assertEqual(config.release_key_compare('/21.0/Athena/x86_64-slc6-gcc62-opt', '/21.0/Athena/x86_64-slc6-gcc62-opt'), 0)
        self.assertGreater(config.release_key_compare('/21.1/Athena/x86_64-slc6-gcc62-opt', '/21.0/Athena/x86_64-slc6-gcc62-opt'), 0)

    def test_none(self):
        """Test a missing configuration file."""
        config = ArtConfiguration()
        self.assertEqual(config.packages(), [])
        self.assertEqual(config.keys('21.0', 'Athena', 'x86_64-slc6-gcc62-opt'), set())
        self.assertEqual(config.get('21.0', 'Athena', 'x86_64-slc6-gcc62-opt', 'Tier0ChainTests', 'dst', ''), '')
        self.assertEqual(config.get_option('21.0', 'Athena', 'x86_64-slc6-gcc62-opt', 'Tier0ChainTests', 'dst', 'dst='), '')

    def test_file(self):
        """Test a configuration file."""
        config = ArtConfiguration(os.path.join(self.test_directory, "data_art_configuration/art-configuration.yml"))
        self.assertEqual(config.release_key('21.0', 'Athena', 'x86_64-slc6-gcc62-opt'), '/21.0/Athena/x86_64-slc6-gcc62-opt')
        self.assertEqual(config.packages(), ['All', 'TrigAnalysisTest', 'SimCoreTests', 'TrigInDetValidation', 'TriggerTest', 'OverlayMonitoringRTT', 'Tier0ChainTests', 'DigitizationTests', 'ISF_Validation'])
        self.assertEqual(config.keys('21.0', 'Athena', 'x86_64-slc6-gcc62-opt'), set(['exclude-sites']))
        self.assertEqual(config.keys('21.0', 'Athena', 'x86_64-slc6-gcc62-opt', 'Tier0ChainTests'), set(['dst', 'copy', 'sites', 'days']))
        self.assertEqual(config.keys('21.0', 'Athena', 'x86_64-slc6-gcc62-opt', 'NonExistingPackage'), set([]))
        self.assertEqual(config.get('21.0', 'Athena', 'x86_64-slc6-gcc62-opt', 'Tier0ChainTests', 'dst', ''), '/eos/atlas/atlascerngroupdisk/data-art/grid-output')
        self.assertEqual(config.get('21.0', 'Athena', 'x86_64-slc6-gcc62-opt', None, 'dst'), None)
        self.assertEqual(config.get('21.0', 'Athena', 'x86_64-slc6-gcc62-opt', 'NonExistingPackage', 'dst'), None)
        self.assertEqual(config.get('21.0', 'Athena', 'x86_64-slc6-gcc62-opt', 'Tier0ChainTests', 'NonExistingKey'), None)
        self.assertEqual(config.get_option('21.0', 'Athena', 'x86_64-slc6-gcc62-opt', 'Tier0ChainTests', 'dst', 'dst='), 'dst=/eos/atlas/atlascerngroupdisk/data-art/grid-output')


if __name__ == '__main__':
    unittest.main()
