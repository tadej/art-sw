#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""TBD."""
import logging
import sys

from ART.art_header import ArtHeader
from ART.art_misc import run_command_parallel

MODULE = "parallel"


log = logging.getLogger(MODULE)
log.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
format_string = "%(asctime)s %(name)15s.%(funcName)-15s %(levelname)8s %(message)s"
date_format_string = None
formatter = logging.Formatter(format_string, date_format_string)
handler.setFormatter(formatter)
log.addHandler(handler)
log.propagate = False

cmd = "./parallel_test.sh"

header = ArtHeader(cmd)
ncores = header.get(ArtHeader.ART_CORES)
if ncores > 1:
    nthreads = header.get(ArtHeader.ART_INPUT_NFILES)

nfiles = 8

nthreads = nfiles
ncores = 4

(exit_code, out, err, command, start_time, end_time) = run_command_parallel(cmd, nthreads, ncores, dir=".")

print exit_code

print out

print err
