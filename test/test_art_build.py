#!/usr/bin/env python
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
"""TBD."""

__author__ = "Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>"

import json
import logging
import os
import shutil
import time
import unittest

from ART.art_build import ArtBuild
from ART.art_testcase import ArtTestCase
from ART.art_misc import set_log_level


class TestArtBuild(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArtBuild, self).__init__(*args, **kwargs)
        self.cwd = os.getcwd()

    def setUp(self):
        """Setting up Build Tests."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)
        self.nightly_release = 'master'
        self.project = 'Athena'
        self.platform = 'x86_64-slc6-gcc62-opt'
        self.nightly_tag = 'latest'

    def check_status_json(self, file, nightly_release, project, platform, nightly_tag):
        """Check status json for consistency."""
        with open(file, "rb") as f:
            status = json.load(f)
        self.assertEqual(status['release_info']['nightly_release'], nightly_release)
        self.assertEqual(status['release_info']['project'], project)
        self.assertEqual(status['release_info']['platform'], platform)
        self.assertEqual(status['release_info']['nightly_tag'], nightly_tag)

    def check_test_status_json(self, file, test_name, description, exit_code, result):
        """Check status json for consistency."""
        with open(file, "rb") as f:
            status = json.load(f)
        package = os.path.basename(file).replace('-status.json', '')
        self.assertEqual(status[package][test_name]['description'], description)
        self.assertEqual(status[package][test_name]['exit_code'], exit_code)
        self.assertEqual(status[package][test_name]['result'], result)

    def check_test_status_json_art_yml(self, file, test_name, report_to):
        """Check status json for consistency."""
        with open(file, "rb") as f:
            status = json.load(f)
        package = os.path.basename(file).replace('-status.json', '')
        self.assertEqual(status[package][test_name]['report-to'], report_to)

    def test_task_list(self):
        """Parallel Running test."""
        seq = 'tmp-unitbuild-build'
        copy = 'tmp-unitbuild-copy'
        timeout = 10
        if os.path.isdir(seq):
            shutil.rmtree(seq)
        if os.path.isdir(copy):
            shutil.rmtree(copy)

        build = ArtBuild(self.art_directory, self.nightly_release, self.project, self.platform, self.nightly_tag, script_directory=os.path.join(self.test_directory, 'data_art_build'), max_jobs=20, ci=False, run_all_tests=True)

        start = time.time()
        self.assertEqual(build.task_list('build', seq, timeout=timeout, copy=copy), -9)
        end = time.time()

        self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-000', 'test_test0', 'stderr.txt')), '')
        self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-000', 'test_test0', 'stdout.txt')), 'Test 000-000\nart-result: [0, 1]\n')
        self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-001', 'test_test1', 'stderr.txt')), '')
        self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-001', 'test_test1', 'stdout.txt')), 'Test 001-001\nart-result: 0\n')
        self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-001', 'test_test2', 'stderr.txt')), '')
        self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-001', 'test_test2', 'stdout.txt')), 'Test 001-002\nart-result: [0, 0, 0]\n')
        self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-002', 'test_test3', 'stderr.txt')), '')
        self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-002', 'test_test3', 'stdout.txt')), 'Test 002-003\nart-result: 0 test_1\nart-result: 0 test number 2\n')
        # self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-002', 'test_test4', 'stderr.txt')), '\nProcess timed out after ' + str(timeout) + ' seconds.\n')
        self.assertEqual(self.read_text(os.path.join(copy, 'Package-build-002', 'test_test4', 'stdout.txt')), 'Test 002-004\n')

        # check status.json
        self.check_status_json(os.path.join(copy, 'status.json'), self.nightly_release, self.project, self.platform, self.nightly_tag)
        self.check_test_status_json(os.path.join(copy, 'Package-build-000-status.json'), 'test_test0.sh', 'Test 0', 0, [{'name': '', 'result': 0}, {'name': '', 'result': 1}])
        self.check_test_status_json_art_yml(os.path.join(copy, 'Package-build-000-status.json'), 'test_test0.sh', {'jira': {'ATLASSIM': 'http://server/link-to-jira-simulation', 'ATR': 'https://server/link-to-jira-to-trigger'}, 'mail': 'Tulay.Cuhadar@cern.ch'})
        self.check_test_status_json(os.path.join(copy, 'Package-build-001-status.json'), 'test_test1.sh', 'Test 1', 0, [{'name': '', 'result': 0}])
        self.check_test_status_json(os.path.join(copy, 'Package-build-001-status.json'), 'test_test2.sh', 'Test 2', 0, [{'name': '', 'result': 0}, {'name': '', 'result': 0}, {'name': '', 'result': 0}])
        self.check_test_status_json(os.path.join(copy, 'Package-build-002-status.json'), 'test_test3.sh', 'Test 3', 0, [{'name': 'test_1', 'result': 0}, {'name': 'test number 2', 'result': 0}])
        self.check_test_status_json(os.path.join(copy, 'Package-build-002-status.json'), 'test_test4.sh', 'Test 4', -9, [])

        # check for empty test (no tests in package should not produce status file)
        self.assertFalse(os.path.isfile(os.path.join(seq, 'Package-build-empty-status.json')))

        # check output files exists
        self.assertTrue(os.path.isfile(os.path.join(copy, 'Package-build-000', 'test_test0', 'data-000-000.output')))
        self.assertTrue(os.path.isfile(os.path.join(copy, 'Package-build-001', 'test_test1', 'data-001-001.output')))
        self.assertTrue(os.path.isfile(os.path.join(copy, 'Package-build-001', 'test_test2', 'data-001-002.output')))
        self.assertTrue(os.path.isfile(os.path.join(copy, 'Package-build-002', 'test_test3', 'data-002-003.output')))
        # timed out package, still copy of data
        self.assertTrue(os.path.isfile(os.path.join(seq, 'Package-build-002', 'test_test4', 'data-002-004.output')))
        self.assertTrue(os.path.isfile(os.path.join(copy, 'Package-build-002', 'test_test4', 'data-002-004.output')))

        # check total time, every job 7 seconds, copy included, timeout on 10, should be around 15
        duration = end - start
        self.assertLess(duration, 18.0)

    def test_task_list_ci(self):
        """Parallel Running ci test."""
        seq = 'tmp-unitbuild-build-ci'
        nightly_release = "21.0"
        project = "Athena"
        platform = "x86_64-slc6-gcc62-opt"
        nightly_tag = "latest"

        if os.path.isdir(seq):
            shutil.rmtree(seq)

        build = ArtBuild(self.art_directory, nightly_release, project, platform, nightly_tag, script_directory=os.path.join(self.test_directory, 'data_art_build'), max_jobs=20, ci=True, run_all_tests=True)

        self.assertEqual(build.task_list('build', seq), 0)

        self.assertEqual(self.read_text(os.path.join(seq, 'Package-build-000', 'test_test0', 'stderr.txt')), '')
        self.assertEqual(self.read_text(os.path.join(seq, 'Package-build-000', 'test_test0', 'stdout.txt')), 'Test 000-000\nart-result: [0, 1]\n')
        self.assertEqual(self.read_text(os.path.join(seq, 'Package-build-001', 'test_test1', 'stderr.txt')), '')
        self.assertEqual(self.read_text(os.path.join(seq, 'Package-build-001', 'test_test1', 'stdout.txt')), 'Test 001-001\nart-result: 0\n')
        self.assertEqual(self.read_text(os.path.join(seq, 'Package-build-002', 'test_test3', 'stderr.txt')), '')
        self.assertEqual(self.read_text(os.path.join(seq, 'Package-build-002', 'test_test3', 'stdout.txt')), 'Test 002-003\nart-result: 0 test_1\nart-result: 0 test number 2\n')

        # check status.json
        self.check_status_json(os.path.join(seq, 'status.json'), nightly_release, project, platform, nightly_tag)
        self.check_test_status_json(os.path.join(seq, 'Package-build-000-status.json'), 'test_test0.sh', 'Test 0', 0, [{'name': '', 'result': 0}, {'name': '', 'result': 1}])
        self.check_test_status_json(os.path.join(seq, 'Package-build-001-status.json'), 'test_test1.sh', 'Test 1', 0, [{'name': '', 'result': 0}])
        self.check_test_status_json(os.path.join(seq, 'Package-build-002-status.json'), 'test_test3.sh', 'Test 3', 0, [{'name': 'test_1', 'result': 0}, {'name': 'test number 2', 'result': 0}])

    def test_task_list_no_tests(self):
        """Parallel Running no tests."""
        seq = 'tmp-unitbuild-build-no-tests'
        nightly_release = "21.0"
        project = "Athena"
        platform = "x86_64-slc6-gcc62-opt"
        nightly_tag = "latest"

        build = ArtBuild(self.art_directory, nightly_release, project, platform, nightly_tag, script_directory=os.path.join(self.test_directory, 'data_art_configuration'), max_jobs=20, ci=False, run_all_tests=True)

        self.assertEqual(build.task_list('build', seq), 0)

    def test_task_list_single_package(self):
        """Parallel Running single package."""
        seq = 'tmp-unitbuild-build-single-package'
        nightly_release = "21.0"
        project = "Athena"
        platform = "x86_64-slc6-gcc62-opt"
        nightly_tag = "latest"

        build = ArtBuild(self.art_directory, nightly_release, project, platform, nightly_tag, script_directory=os.path.join(self.test_directory, 'data_art_build', 'Package-build-001'), max_jobs=20, ci=False, run_all_tests=True)

        self.assertEqual(build.task_list('build', seq), 0)

    def test_task_list_timeout(self):
        """Parallel Running test, timeout should be exact, not per job."""
        seq = 'tmp-unitbuild-build'
        timeout = 2
        if os.path.isdir(seq):
            shutil.rmtree(seq)

        build = ArtBuild(self.art_directory, self.nightly_release, self.project, self.platform, self.nightly_tag, script_directory=os.path.join(self.test_directory, 'data_art_build'), max_jobs=20, ci=False, run_all_tests=True)

        start = time.time()
        self.assertEqual(build.task_list('build', seq, timeout=timeout), -9)
        end = time.time()

        # check total time, every job 7 seconds, copy included, timeout on 3, should be around 15
        duration = end - start
        print "duration", duration
        self.assertLess(duration, 18.0)


if __name__ == '__main__':
    unittest.main()
